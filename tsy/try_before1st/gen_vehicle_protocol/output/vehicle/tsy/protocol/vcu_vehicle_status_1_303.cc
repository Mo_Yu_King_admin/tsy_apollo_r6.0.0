/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/vcu_vehicle_status_1_303.h"

#include "modules/drivers/canbus/common/byte.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

const int32_t Vcuvehiclestatus1303::ID = 0x303;

// public
Vcuvehiclestatus1303::Vcuvehiclestatus1303() { Reset(); }

uint32_t Vcuvehiclestatus1303::GetPeriod() const {
  // TODO(All) :  modify every protocol's period manually
  static const uint32_t PERIOD = 20 * 1000;
  return PERIOD;
}

void Vcuvehiclestatus1303::UpdateData(uint8_t* data) {
  set_p_epb_status_feedback(data, epb_status_feedback_);
  set_p_vehicle_status_reserved(data, vehicle_status_reserved_);
  set_p_vehicle_voltage(data, vehicle_voltage_);
  set_p_bms_status(data, bms_status_);
  set_p_vcu_ready_flag(data, vcu_ready_flag_);
  set_p_ignitionstatus(data, ignitionstatus_);
}

void Vcuvehiclestatus1303::Reset() {
  // TODO(All) :  you should check this manually
  epb_status_feedback_ = 0;
  vehicle_status_reserved_ = 0;
  vehicle_voltage_ = 0.0;
  bms_status_ = 0;
  vcu_ready_flag_ = 0;
  ignitionstatus_ = 0;
}

Vcuvehiclestatus1303* Vcuvehiclestatus1303::set_epb_status_feedback(
    int epb_status_feedback) {
  epb_status_feedback_ = epb_status_feedback;
  return this;
 }

// config detail: {'bit': 24, 'is_signed_var': False, 'len': 8, 'name': 'EPB_Status_FeedBack', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclestatus1303::set_p_epb_status_feedback(uint8_t* data,
    int epb_status_feedback) {
  epb_status_feedback = ProtocolData::BoundedValue(0, 3, epb_status_feedback);
  int x = epb_status_feedback;

  Byte to_set(data + 3);
  to_set.set_value(x, 0, 8);
}


Vcuvehiclestatus1303* Vcuvehiclestatus1303::set_vehicle_status_reserved(
    int vehicle_status_reserved) {
  vehicle_status_reserved_ = vehicle_status_reserved;
  return this;
 }

// config detail: {'bit': 48, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Status_Reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclestatus1303::set_p_vehicle_status_reserved(uint8_t* data,
    int vehicle_status_reserved) {
  vehicle_status_reserved = ProtocolData::BoundedValue(0, 255, vehicle_status_reserved);
  int x = vehicle_status_reserved;
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 6);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 7);
  to_set1.set_value(t, 0, 8);
}


Vcuvehiclestatus1303* Vcuvehiclestatus1303::set_vehicle_voltage(
    double vehicle_voltage) {
  vehicle_voltage_ = vehicle_voltage;
  return this;
 }

// config detail: {'bit': 32, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Voltage', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'V', 'precision': 0.1, 'type': 'double'}
void Vcuvehiclestatus1303::set_p_vehicle_voltage(uint8_t* data,
    double vehicle_voltage) {
  vehicle_voltage = ProtocolData::BoundedValue(0.0, 100.0, vehicle_voltage);
  int x = vehicle_voltage / 0.100000;
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 4);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 5);
  to_set1.set_value(t, 0, 8);
}


Vcuvehiclestatus1303* Vcuvehiclestatus1303::set_bms_status(
    int bms_status) {
  bms_status_ = bms_status;
  return this;
 }

// config detail: {'bit': 16, 'is_signed_var': False, 'len': 8, 'name': 'BMS_Status', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclestatus1303::set_p_bms_status(uint8_t* data,
    int bms_status) {
  bms_status = ProtocolData::BoundedValue(0, 255, bms_status);
  int x = bms_status;

  Byte to_set(data + 2);
  to_set.set_value(x, 0, 8);
}


Vcuvehiclestatus1303* Vcuvehiclestatus1303::set_vcu_ready_flag(
    int vcu_ready_flag) {
  vcu_ready_flag_ = vcu_ready_flag;
  return this;
 }

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Ready_Flag', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclestatus1303::set_p_vcu_ready_flag(uint8_t* data,
    int vcu_ready_flag) {
  vcu_ready_flag = ProtocolData::BoundedValue(0, 1, vcu_ready_flag);
  int x = vcu_ready_flag;

  Byte to_set(data + 0);
  to_set.set_value(x, 0, 8);
}


Vcuvehiclestatus1303* Vcuvehiclestatus1303::set_ignitionstatus(
    int ignitionstatus) {
  ignitionstatus_ = ignitionstatus;
  return this;
 }

// config detail: {'bit': 8, 'is_signed_var': False, 'len': 8, 'name': 'IgnitionStatus', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclestatus1303::set_p_ignitionstatus(uint8_t* data,
    int ignitionstatus) {
  ignitionstatus = ProtocolData::BoundedValue(0, 1, ignitionstatus);
  int x = ignitionstatus;

  Byte to_set(data + 1);
  to_set.set_value(x, 0, 8);
}

}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
