/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/vcu_vehicle_diagnosis_301.h"

#include "modules/drivers/canbus/common/byte.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

const int32_t Vcuvehiclediagnosis301::ID = 0x301;

// public
Vcuvehiclediagnosis301::Vcuvehiclediagnosis301() { Reset(); }

uint32_t Vcuvehiclediagnosis301::GetPeriod() const {
  // TODO(All) :  modify every protocol's period manually
  static const uint32_t PERIOD = 20 * 1000;
  return PERIOD;
}

void Vcuvehiclediagnosis301::UpdateData(uint8_t* data) {
  set_p_vehicle_diagnosis_reserved_3(data, vehicle_diagnosis_reserved_3_);
  set_p_vehicle_diagnosis_reserved_1(data, vehicle_diagnosis_reserved_1_);
  set_p_vehicle_diagnosis_reserved_2(data, vehicle_diagnosis_reserved_2_);
  set_p_epb_state(data, epb_state_);
  set_p_vehicle_fault_grade(data, vehicle_fault_grade_);
  set_p_r_attach_switch_state(data, r_attach_switch_state_);
  set_p_f_attach_switch_state(data, f_attach_switch_state_);
  set_p_bms_state(data, bms_state_);
  set_p_iecu_state(data, iecu_state_);
  set_p_dbsr_state(data, dbsr_state_);
  set_p_rlmotor_state(data, rlmotor_state_);
  set_p_rrmotor_state(data, rrmotor_state_);
  set_p_flmotor_state(data, flmotor_state_);
  set_p_frmotor_state(data, frmotor_state_);
  set_p_rsteering_state(data, rsteering_state_);
  set_p_fsteering_state(data, fsteering_state_);
  set_p_dbsf_state(data, dbsf_state_);
  set_p_remote_state(data, remote_state_);
  set_p_emergency_button_state(data, emergency_button_state_);
}

void Vcuvehiclediagnosis301::Reset() {
  // TODO(All) :  you should check this manually
  vehicle_diagnosis_reserved_3_ = 0;
  vehicle_diagnosis_reserved_1_ = 0;
  vehicle_diagnosis_reserved_2_ = 0;
  epb_state_ = false;
  vehicle_fault_grade_ = 0;
  r_attach_switch_state_ = false;
  f_attach_switch_state_ = false;
  bms_state_ = false;
  iecu_state_ = false;
  dbsr_state_ = false;
  rlmotor_state_ = false;
  rrmotor_state_ = false;
  flmotor_state_ = false;
  frmotor_state_ = false;
  rsteering_state_ = false;
  fsteering_state_ = false;
  dbsf_state_ = false;
  remote_state_ = false;
  emergency_button_state_ = false;
}

Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_vehicle_diagnosis_reserved_3(
    int vehicle_diagnosis_reserved_3) {
  vehicle_diagnosis_reserved_3_ = vehicle_diagnosis_reserved_3;
  return this;
 }

// config detail: {'bit': 48, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Diagnosis_Reserved_3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclediagnosis301::set_p_vehicle_diagnosis_reserved_3(uint8_t* data,
    int vehicle_diagnosis_reserved_3) {
  vehicle_diagnosis_reserved_3 = ProtocolData::BoundedValue(0, 255, vehicle_diagnosis_reserved_3);
  int x = vehicle_diagnosis_reserved_3;
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 6);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 7);
  to_set1.set_value(t, 0, 8);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_vehicle_diagnosis_reserved_1(
    int vehicle_diagnosis_reserved_1) {
  vehicle_diagnosis_reserved_1_ = vehicle_diagnosis_reserved_1;
  return this;
 }

// config detail: {'bit': 18, 'is_signed_var': False, 'len': 14, 'name': 'Vehicle_Diagnosis_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|0]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclediagnosis301::set_p_vehicle_diagnosis_reserved_1(uint8_t* data,
    int vehicle_diagnosis_reserved_1) {
  vehicle_diagnosis_reserved_1 = ProtocolData::BoundedValue(0, 0, vehicle_diagnosis_reserved_1);
  int x = vehicle_diagnosis_reserved_1;
  uint8_t t = 0;

  t = x & 0x3F;
  Byte to_set0(data + 2);
  to_set0.set_value(t, 2, 6);
  x >>= 6;

  t = x & 0xFF;
  Byte to_set1(data + 3);
  to_set1.set_value(t, 0, 8);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_vehicle_diagnosis_reserved_2(
    int vehicle_diagnosis_reserved_2) {
  vehicle_diagnosis_reserved_2_ = vehicle_diagnosis_reserved_2;
  return this;
 }

// config detail: {'bit': 32, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Diagnosis_Reserved_2', 'offset': -0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclediagnosis301::set_p_vehicle_diagnosis_reserved_2(uint8_t* data,
    int vehicle_diagnosis_reserved_2) {
  vehicle_diagnosis_reserved_2 = ProtocolData::BoundedValue(0, 255, vehicle_diagnosis_reserved_2);
  int x = vehicle_diagnosis_reserved_2;
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 4);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 5);
  to_set1.set_value(t, 0, 8);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_epb_state(
    bool epb_state) {
  epb_state_ = epb_state;
  return this;
 }

// config detail: {'bit': 14, 'is_signed_var': False, 'len': 1, 'name': 'EPB_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_epb_state(uint8_t* data,
    bool epb_state) {
  int x = epb_state;

  Byte to_set(data + 1);
  to_set.set_value(x, 6, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_vehicle_fault_grade(
    int vehicle_fault_grade) {
  vehicle_fault_grade_ = vehicle_fault_grade;
  return this;
 }

// config detail: {'bit': 15, 'is_signed_var': False, 'len': 2, 'name': 'Vehicle_Fault_Grade', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuvehiclediagnosis301::set_p_vehicle_fault_grade(uint8_t* data,
    int vehicle_fault_grade) {
  vehicle_fault_grade = ProtocolData::BoundedValue(0, 3, vehicle_fault_grade);
  int x = vehicle_fault_grade;
  uint8_t t = 0;

  t = x & 0x1;
  Byte to_set0(data + 1);
  to_set0.set_value(t, 7, 1);
  x >>= 1;

  t = x & 0x1;
  Byte to_set1(data + 2);
  to_set1.set_value(t, 0, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_r_attach_switch_state(
    bool r_attach_switch_state) {
  r_attach_switch_state_ = r_attach_switch_state;
  return this;
 }

// config detail: {'bit': 13, 'is_signed_var': False, 'len': 1, 'name': 'R_Attach_Switch_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_r_attach_switch_state(uint8_t* data,
    bool r_attach_switch_state) {
  int x = r_attach_switch_state;

  Byte to_set(data + 1);
  to_set.set_value(x, 5, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_f_attach_switch_state(
    bool f_attach_switch_state) {
  f_attach_switch_state_ = f_attach_switch_state;
  return this;
 }

// config detail: {'bit': 12, 'is_signed_var': False, 'len': 1, 'name': 'F_Attach_Switch_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_f_attach_switch_state(uint8_t* data,
    bool f_attach_switch_state) {
  int x = f_attach_switch_state;

  Byte to_set(data + 1);
  to_set.set_value(x, 4, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_bms_state(
    bool bms_state) {
  bms_state_ = bms_state;
  return this;
 }

// config detail: {'bit': 9, 'is_signed_var': False, 'len': 1, 'name': 'BMS_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_bms_state(uint8_t* data,
    bool bms_state) {
  int x = bms_state;

  Byte to_set(data + 1);
  to_set.set_value(x, 1, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_iecu_state(
    bool iecu_state) {
  iecu_state_ = iecu_state;
  return this;
 }

// config detail: {'bit': 6, 'is_signed_var': False, 'len': 1, 'name': 'iECU_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_iecu_state(uint8_t* data,
    bool iecu_state) {
  int x = iecu_state;

  Byte to_set(data + 0);
  to_set.set_value(x, 6, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_dbsr_state(
    bool dbsr_state) {
  dbsr_state_ = dbsr_state;
  return this;
 }

// config detail: {'bit': 8, 'is_signed_var': False, 'len': 1, 'name': 'DBSR_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_dbsr_state(uint8_t* data,
    bool dbsr_state) {
  int x = dbsr_state;

  Byte to_set(data + 1);
  to_set.set_value(x, 0, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_rlmotor_state(
    bool rlmotor_state) {
  rlmotor_state_ = rlmotor_state;
  return this;
 }

// config detail: {'bit': 4, 'is_signed_var': False, 'len': 1, 'name': 'RLMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_rlmotor_state(uint8_t* data,
    bool rlmotor_state) {
  int x = rlmotor_state;

  Byte to_set(data + 0);
  to_set.set_value(x, 4, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_rrmotor_state(
    bool rrmotor_state) {
  rrmotor_state_ = rrmotor_state;
  return this;
 }

// config detail: {'bit': 3, 'is_signed_var': False, 'len': 1, 'name': 'RRMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_rrmotor_state(uint8_t* data,
    bool rrmotor_state) {
  int x = rrmotor_state;

  Byte to_set(data + 0);
  to_set.set_value(x, 3, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_flmotor_state(
    bool flmotor_state) {
  flmotor_state_ = flmotor_state;
  return this;
 }

// config detail: {'bit': 2, 'is_signed_var': False, 'len': 1, 'name': 'FLMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_flmotor_state(uint8_t* data,
    bool flmotor_state) {
  int x = flmotor_state;

  Byte to_set(data + 0);
  to_set.set_value(x, 2, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_frmotor_state(
    bool frmotor_state) {
  frmotor_state_ = frmotor_state;
  return this;
 }

// config detail: {'bit': 1, 'is_signed_var': False, 'len': 1, 'name': 'FRMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_frmotor_state(uint8_t* data,
    bool frmotor_state) {
  int x = frmotor_state;

  Byte to_set(data + 0);
  to_set.set_value(x, 1, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_rsteering_state(
    bool rsteering_state) {
  rsteering_state_ = rsteering_state;
  return this;
 }

// config detail: {'bit': 11, 'is_signed_var': False, 'len': 1, 'name': 'RSteering_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_rsteering_state(uint8_t* data,
    bool rsteering_state) {
  int x = rsteering_state;

  Byte to_set(data + 1);
  to_set.set_value(x, 3, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_fsteering_state(
    bool fsteering_state) {
  fsteering_state_ = fsteering_state;
  return this;
 }

// config detail: {'bit': 10, 'is_signed_var': False, 'len': 1, 'name': 'FSteering_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_fsteering_state(uint8_t* data,
    bool fsteering_state) {
  int x = fsteering_state;

  Byte to_set(data + 1);
  to_set.set_value(x, 2, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_dbsf_state(
    bool dbsf_state) {
  dbsf_state_ = dbsf_state;
  return this;
 }

// config detail: {'bit': 7, 'is_signed_var': False, 'len': 1, 'name': 'DBSF_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_dbsf_state(uint8_t* data,
    bool dbsf_state) {
  int x = dbsf_state;

  Byte to_set(data + 0);
  to_set.set_value(x, 7, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_remote_state(
    bool remote_state) {
  remote_state_ = remote_state;
  return this;
 }

// config detail: {'bit': 5, 'is_signed_var': False, 'len': 1, 'name': 'Remote_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_remote_state(uint8_t* data,
    bool remote_state) {
  int x = remote_state;

  Byte to_set(data + 0);
  to_set.set_value(x, 5, 1);
}


Vcuvehiclediagnosis301* Vcuvehiclediagnosis301::set_emergency_button_state(
    bool emergency_button_state) {
  emergency_button_state_ = emergency_button_state;
  return this;
 }

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'Emergency_Button_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcuvehiclediagnosis301::set_p_emergency_button_state(uint8_t* data,
    bool emergency_button_state) {
  int x = emergency_button_state;

  Byte to_set(data + 0);
  to_set.set_value(x, 0, 1);
}

}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
