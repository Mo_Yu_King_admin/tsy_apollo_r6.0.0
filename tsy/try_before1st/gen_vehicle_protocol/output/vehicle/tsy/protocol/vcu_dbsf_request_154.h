/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace TSY {

class Vcudbsfrequest154 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;

  Vcudbsfrequest154();

  uint32_t GetPeriod() const override;

  void UpdateData(uint8_t* data) override;

  void Reset() override;

  // config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'VCU_DBSF_Reserved_3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcudbsfrequest154* set_vcu_dbsf_reserved_3(int vcu_dbsf_reserved_3);

  // config detail: {'bit': 18, 'is_signed_var': False, 'len': 22, 'name': 'VCU_DBSF_Reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcudbsfrequest154* set_vcu_dbsf_reserved_2(int vcu_dbsf_reserved_2);

  // config detail: {'bit': 40, 'is_signed_var': False, 'len': 16, 'name': 'VCU_DBSF_Pressure_Request', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|8]', 'physical_unit': 'Mpa', 'precision': 0.016, 'type': 'double'}
  Vcudbsfrequest154* set_vcu_dbsf_pressure_request(double vcu_dbsf_pressure_request);

  // config detail: {'bit': 17, 'is_signed_var': False, 'len': 1, 'name': 'VCU_DBSF_Request_Flag', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcudbsfrequest154* set_vcu_dbsf_request_flag(bool vcu_dbsf_request_flag);

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 17, 'name': 'VCU_DBSF_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|500]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcudbsfrequest154* set_vcu_dbsf_reserved_1(int vcu_dbsf_reserved_1);

 private:

  // config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'VCU_DBSF_Reserved_3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_dbsf_reserved_3(uint8_t* data, int vcu_dbsf_reserved_3);

  // config detail: {'bit': 18, 'is_signed_var': False, 'len': 22, 'name': 'VCU_DBSF_Reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_dbsf_reserved_2(uint8_t* data, int vcu_dbsf_reserved_2);

  // config detail: {'bit': 40, 'is_signed_var': False, 'len': 16, 'name': 'VCU_DBSF_Pressure_Request', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|8]', 'physical_unit': 'Mpa', 'precision': 0.016, 'type': 'double'}
  void set_p_vcu_dbsf_pressure_request(uint8_t* data, double vcu_dbsf_pressure_request);

  // config detail: {'bit': 17, 'is_signed_var': False, 'len': 1, 'name': 'VCU_DBSF_Request_Flag', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_vcu_dbsf_request_flag(uint8_t* data, bool vcu_dbsf_request_flag);

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 17, 'name': 'VCU_DBSF_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|500]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_dbsf_reserved_1(uint8_t* data, int vcu_dbsf_reserved_1);

 private:
  int vcu_dbsf_reserved_3_;
  int vcu_dbsf_reserved_2_;
  double vcu_dbsf_pressure_request_;
  bool vcu_dbsf_request_flag_;
  int vcu_dbsf_reserved_1_;
};

}  // namespace TSY
}  // namespace canbus
}  // namespace apollo


