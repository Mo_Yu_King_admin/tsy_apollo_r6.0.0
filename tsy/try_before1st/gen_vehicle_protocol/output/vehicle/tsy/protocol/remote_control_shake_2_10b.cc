/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/remote_control_shake_2_10b.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

Remotecontrolshake210b::Remotecontrolshake210b() {}
const int32_t Remotecontrolshake210b::ID = 0x10B;

void Remotecontrolshake210b::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_TSY()->mutable_remote_control_shake_2_10b()->set_remote_x1_reserved(remote_x1_reserved(bytes, length));
  chassis->mutable_TSY()->mutable_remote_control_shake_2_10b()->set_remote_y1_longitudinal_control(remote_y1_longitudinal_control(bytes, length));
  chassis->mutable_TSY()->mutable_remote_control_shake_2_10b()->set_remote_y2_reserved(remote_y2_reserved(bytes, length));
  chassis->mutable_TSY()->mutable_remote_control_shake_2_10b()->set_remote_x2_lateral_control(remote_x2_lateral_control(bytes, length));
}

// config detail: {'bit': 48, 'is_signed_var': True, 'len': 16, 'name': 'remote_x1_reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-450|450]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Remotecontrolshake210b::remote_x1_reserved(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 6);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  int ret = x;
  return ret;
}

// config detail: {'bit': 32, 'is_signed_var': True, 'len': 16, 'name': 'remote_y1_longitudinal_control', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-450|450]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Remotecontrolshake210b::remote_y1_longitudinal_control(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 5);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 4);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  int ret = x;
  return ret;
}

// config detail: {'bit': 16, 'is_signed_var': True, 'len': 16, 'name': 'remote_y2_reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-450|450]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Remotecontrolshake210b::remote_y2_reserved(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 3);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 2);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': True, 'len': 16, 'name': 'remote_x2_lateral_control', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-450|450]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Remotecontrolshake210b::remote_x2_lateral_control(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  int ret = x;
  return ret;
}
}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
