/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/iecu_control_distributed_505.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

Iecucontroldistributed505::Iecucontroldistributed505() {}
const int32_t Iecucontroldistributed505::ID = 0x505;

void Iecucontroldistributed505::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_TSY()->mutable_iecu_control_distributed_505()->set_iecu_power_rr(iecu_power_rr(bytes, length));
  chassis->mutable_TSY()->mutable_iecu_control_distributed_505()->set_iecu_power_rl(iecu_power_rl(bytes, length));
  chassis->mutable_TSY()->mutable_iecu_control_distributed_505()->set_iecu_power_fr(iecu_power_fr(bytes, length));
  chassis->mutable_TSY()->mutable_iecu_control_distributed_505()->set_iecu_power_fl(iecu_power_fl(bytes, length));
}

// config detail: {'bit': 48, 'is_signed_var': False, 'len': 16, 'name': 'iecu_power_rr', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|800]', 'physical_unit': 'Rpm/Nm', 'precision': 1.0, 'type': 'int'}
int Iecucontroldistributed505::iecu_power_rr(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 6);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 32, 'is_signed_var': False, 'len': 16, 'name': 'iecu_power_rl', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|800]', 'physical_unit': 'Rpm/Nm', 'precision': 1.0, 'type': 'int'}
int Iecucontroldistributed505::iecu_power_rl(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 5);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 4);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 16, 'is_signed_var': False, 'len': 16, 'name': 'iecu_power_fr', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|800]', 'physical_unit': 'Rpm/Nm', 'precision': 1.0, 'type': 'int'}
int Iecucontroldistributed505::iecu_power_fr(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 3);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 2);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'iecu_power_fl', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|800]', 'physical_unit': 'Rpm/Nm', 'precision': 1.0, 'type': 'int'}
int Iecucontroldistributed505::iecu_power_fl(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}
}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
