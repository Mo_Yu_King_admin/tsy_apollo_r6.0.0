/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace TSY {

class Vcuvehiclediagnosis301 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;

  Vcuvehiclediagnosis301();

  uint32_t GetPeriod() const override;

  void UpdateData(uint8_t* data) override;

  void Reset() override;

  // config detail: {'bit': 48, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Diagnosis_Reserved_3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclediagnosis301* set_vehicle_diagnosis_reserved_3(int vehicle_diagnosis_reserved_3);

  // config detail: {'bit': 18, 'is_signed_var': False, 'len': 14, 'name': 'Vehicle_Diagnosis_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|0]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclediagnosis301* set_vehicle_diagnosis_reserved_1(int vehicle_diagnosis_reserved_1);

  // config detail: {'bit': 32, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Diagnosis_Reserved_2', 'offset': -0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclediagnosis301* set_vehicle_diagnosis_reserved_2(int vehicle_diagnosis_reserved_2);

  // config detail: {'bit': 14, 'is_signed_var': False, 'len': 1, 'name': 'EPB_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_epb_state(bool epb_state);

  // config detail: {'bit': 15, 'is_signed_var': False, 'len': 2, 'name': 'Vehicle_Fault_Grade', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclediagnosis301* set_vehicle_fault_grade(int vehicle_fault_grade);

  // config detail: {'bit': 13, 'is_signed_var': False, 'len': 1, 'name': 'R_Attach_Switch_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_r_attach_switch_state(bool r_attach_switch_state);

  // config detail: {'bit': 12, 'is_signed_var': False, 'len': 1, 'name': 'F_Attach_Switch_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_f_attach_switch_state(bool f_attach_switch_state);

  // config detail: {'bit': 9, 'is_signed_var': False, 'len': 1, 'name': 'BMS_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_bms_state(bool bms_state);

  // config detail: {'bit': 6, 'is_signed_var': False, 'len': 1, 'name': 'iECU_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_iecu_state(bool iecu_state);

  // config detail: {'bit': 8, 'is_signed_var': False, 'len': 1, 'name': 'DBSR_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_dbsr_state(bool dbsr_state);

  // config detail: {'bit': 4, 'is_signed_var': False, 'len': 1, 'name': 'RLMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_rlmotor_state(bool rlmotor_state);

  // config detail: {'bit': 3, 'is_signed_var': False, 'len': 1, 'name': 'RRMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_rrmotor_state(bool rrmotor_state);

  // config detail: {'bit': 2, 'is_signed_var': False, 'len': 1, 'name': 'FLMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_flmotor_state(bool flmotor_state);

  // config detail: {'bit': 1, 'is_signed_var': False, 'len': 1, 'name': 'FRMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_frmotor_state(bool frmotor_state);

  // config detail: {'bit': 11, 'is_signed_var': False, 'len': 1, 'name': 'RSteering_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_rsteering_state(bool rsteering_state);

  // config detail: {'bit': 10, 'is_signed_var': False, 'len': 1, 'name': 'FSteering_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_fsteering_state(bool fsteering_state);

  // config detail: {'bit': 7, 'is_signed_var': False, 'len': 1, 'name': 'DBSF_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_dbsf_state(bool dbsf_state);

  // config detail: {'bit': 5, 'is_signed_var': False, 'len': 1, 'name': 'Remote_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_remote_state(bool remote_state);

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'Emergency_Button_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  Vcuvehiclediagnosis301* set_emergency_button_state(bool emergency_button_state);

 private:

  // config detail: {'bit': 48, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Diagnosis_Reserved_3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vehicle_diagnosis_reserved_3(uint8_t* data, int vehicle_diagnosis_reserved_3);

  // config detail: {'bit': 18, 'is_signed_var': False, 'len': 14, 'name': 'Vehicle_Diagnosis_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|0]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vehicle_diagnosis_reserved_1(uint8_t* data, int vehicle_diagnosis_reserved_1);

  // config detail: {'bit': 32, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Diagnosis_Reserved_2', 'offset': -0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vehicle_diagnosis_reserved_2(uint8_t* data, int vehicle_diagnosis_reserved_2);

  // config detail: {'bit': 14, 'is_signed_var': False, 'len': 1, 'name': 'EPB_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_epb_state(uint8_t* data, bool epb_state);

  // config detail: {'bit': 15, 'is_signed_var': False, 'len': 2, 'name': 'Vehicle_Fault_Grade', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vehicle_fault_grade(uint8_t* data, int vehicle_fault_grade);

  // config detail: {'bit': 13, 'is_signed_var': False, 'len': 1, 'name': 'R_Attach_Switch_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_r_attach_switch_state(uint8_t* data, bool r_attach_switch_state);

  // config detail: {'bit': 12, 'is_signed_var': False, 'len': 1, 'name': 'F_Attach_Switch_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_f_attach_switch_state(uint8_t* data, bool f_attach_switch_state);

  // config detail: {'bit': 9, 'is_signed_var': False, 'len': 1, 'name': 'BMS_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_bms_state(uint8_t* data, bool bms_state);

  // config detail: {'bit': 6, 'is_signed_var': False, 'len': 1, 'name': 'iECU_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_iecu_state(uint8_t* data, bool iecu_state);

  // config detail: {'bit': 8, 'is_signed_var': False, 'len': 1, 'name': 'DBSR_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_dbsr_state(uint8_t* data, bool dbsr_state);

  // config detail: {'bit': 4, 'is_signed_var': False, 'len': 1, 'name': 'RLMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_rlmotor_state(uint8_t* data, bool rlmotor_state);

  // config detail: {'bit': 3, 'is_signed_var': False, 'len': 1, 'name': 'RRMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_rrmotor_state(uint8_t* data, bool rrmotor_state);

  // config detail: {'bit': 2, 'is_signed_var': False, 'len': 1, 'name': 'FLMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_flmotor_state(uint8_t* data, bool flmotor_state);

  // config detail: {'bit': 1, 'is_signed_var': False, 'len': 1, 'name': 'FRMotor_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_frmotor_state(uint8_t* data, bool frmotor_state);

  // config detail: {'bit': 11, 'is_signed_var': False, 'len': 1, 'name': 'RSteering_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_rsteering_state(uint8_t* data, bool rsteering_state);

  // config detail: {'bit': 10, 'is_signed_var': False, 'len': 1, 'name': 'FSteering_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_fsteering_state(uint8_t* data, bool fsteering_state);

  // config detail: {'bit': 7, 'is_signed_var': False, 'len': 1, 'name': 'DBSF_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_dbsf_state(uint8_t* data, bool dbsf_state);

  // config detail: {'bit': 5, 'is_signed_var': False, 'len': 1, 'name': 'Remote_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_remote_state(uint8_t* data, bool remote_state);

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'Emergency_Button_State', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
  void set_p_emergency_button_state(uint8_t* data, bool emergency_button_state);

 private:
  int vehicle_diagnosis_reserved_3_;
  int vehicle_diagnosis_reserved_1_;
  int vehicle_diagnosis_reserved_2_;
  bool epb_state_;
  int vehicle_fault_grade_;
  bool r_attach_switch_state_;
  bool f_attach_switch_state_;
  bool bms_state_;
  bool iecu_state_;
  bool dbsr_state_;
  bool rlmotor_state_;
  bool rrmotor_state_;
  bool flmotor_state_;
  bool frmotor_state_;
  bool rsteering_state_;
  bool fsteering_state_;
  bool dbsf_state_;
  bool remote_state_;
  bool emergency_button_state_;
};

}  // namespace TSY
}  // namespace canbus
}  // namespace apollo


