/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/vcu_epsf_control_request_469.h"

#include "modules/drivers/canbus/common/byte.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

const int32_t Vcuepsfcontrolrequest469::ID = 0x469;

// public
Vcuepsfcontrolrequest469::Vcuepsfcontrolrequest469() { Reset(); }

uint32_t Vcuepsfcontrolrequest469::GetPeriod() const {
  // TODO(All) :  modify every protocol's period manually
  static const uint32_t PERIOD = 20 * 1000;
  return PERIOD;
}

void Vcuepsfcontrolrequest469::UpdateData(uint8_t* data) {
  set_p_vcu_control_epsf_reserved_2(data, vcu_control_epsf_reserved_2_);
  set_p_vcu_epsf_checksum(data, vcu_epsf_checksum_);
  set_p_vcu_request_epsf_angle_speed(data, vcu_request_epsf_angle_speed_);
  set_p_vcu_request_epsf_angle_calibrate(data, vcu_request_epsf_angle_calibrate_);
  set_p_low_vcu_req_epsf_target_angle(data, low_vcu_req_epsf_target_angle_);
  set_p_high_vcu_req_epsf_target_angle(data, high_vcu_req_epsf_target_angle_);
  set_p_vcu_control_epsf_reserved_1(data, vcu_control_epsf_reserved_1_);
  set_p_vcu_request_epsf_control_mode(data, vcu_request_epsf_control_mode_);
}

void Vcuepsfcontrolrequest469::Reset() {
  // TODO(All) :  you should check this manually
  vcu_control_epsf_reserved_2_ = 0;
  vcu_epsf_checksum_ = 0;
  vcu_request_epsf_angle_speed_ = 0;
  vcu_request_epsf_angle_calibrate_ = 0;
  low_vcu_req_epsf_target_angle_ = 0;
  high_vcu_req_epsf_target_angle_ = 0;
  vcu_control_epsf_reserved_1_ = 0;
  vcu_request_epsf_control_mode_ = 0;
}

Vcuepsfcontrolrequest469* Vcuepsfcontrolrequest469::set_vcu_control_epsf_reserved_2(
    int vcu_control_epsf_reserved_2) {
  vcu_control_epsf_reserved_2_ = vcu_control_epsf_reserved_2;
  return this;
 }

// config detail: {'bit': 16, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Control_EPSF_Reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuepsfcontrolrequest469::set_p_vcu_control_epsf_reserved_2(uint8_t* data,
    int vcu_control_epsf_reserved_2) {
  vcu_control_epsf_reserved_2 = ProtocolData::BoundedValue(0, 255, vcu_control_epsf_reserved_2);
  int x = vcu_control_epsf_reserved_2;

  Byte to_set(data + 2);
  to_set.set_value(x, 0, 8);
}


Vcuepsfcontrolrequest469* Vcuepsfcontrolrequest469::set_vcu_epsf_checksum(
    int vcu_epsf_checksum) {
  vcu_epsf_checksum_ = vcu_epsf_checksum;
  return this;
 }

// config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'VCU_EPSF_CheckSum', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuepsfcontrolrequest469::set_p_vcu_epsf_checksum(uint8_t* data,
    int vcu_epsf_checksum) {
  vcu_epsf_checksum = ProtocolData::BoundedValue(0, 255, vcu_epsf_checksum);
  int x = vcu_epsf_checksum;

  Byte to_set(data + 7);
  to_set.set_value(x, 0, 8);
}


Vcuepsfcontrolrequest469* Vcuepsfcontrolrequest469::set_vcu_request_epsf_angle_speed(
    int vcu_request_epsf_angle_speed) {
  vcu_request_epsf_angle_speed_ = vcu_request_epsf_angle_speed;
  return this;
 }

// config detail: {'bit': 48, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Angle_Speed', 'offset': 0.0, 'order': 'intel', 'physical_range': '[20|250]', 'physical_unit': 'deg/s', 'precision': 1.0, 'type': 'int'}
void Vcuepsfcontrolrequest469::set_p_vcu_request_epsf_angle_speed(uint8_t* data,
    int vcu_request_epsf_angle_speed) {
  vcu_request_epsf_angle_speed = ProtocolData::BoundedValue(20, 250, vcu_request_epsf_angle_speed);
  int x = vcu_request_epsf_angle_speed;

  Byte to_set(data + 6);
  to_set.set_value(x, 0, 8);
}


Vcuepsfcontrolrequest469* Vcuepsfcontrolrequest469::set_vcu_request_epsf_angle_calibrate(
    int vcu_request_epsf_angle_calibrate) {
  vcu_request_epsf_angle_calibrate_ = vcu_request_epsf_angle_calibrate;
  return this;
 }

// config detail: {'bit': 40, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Angle_Calibrate', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuepsfcontrolrequest469::set_p_vcu_request_epsf_angle_calibrate(uint8_t* data,
    int vcu_request_epsf_angle_calibrate) {
  vcu_request_epsf_angle_calibrate = ProtocolData::BoundedValue(0, 255, vcu_request_epsf_angle_calibrate);
  int x = vcu_request_epsf_angle_calibrate;

  Byte to_set(data + 5);
  to_set.set_value(x, 0, 8);
}


Vcuepsfcontrolrequest469* Vcuepsfcontrolrequest469::set_low_vcu_req_epsf_target_angle(
    int low_vcu_req_epsf_target_angle) {
  low_vcu_req_epsf_target_angle_ = low_vcu_req_epsf_target_angle;
  return this;
 }

// config detail: {'bit': 32, 'is_signed_var': False, 'len': 8, 'name': 'Low_VCU_Req_EPSF_Target_Angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'deg', 'precision': 1.0, 'type': 'int'}
void Vcuepsfcontrolrequest469::set_p_low_vcu_req_epsf_target_angle(uint8_t* data,
    int low_vcu_req_epsf_target_angle) {
  low_vcu_req_epsf_target_angle = ProtocolData::BoundedValue(0, 255, low_vcu_req_epsf_target_angle);
  int x = low_vcu_req_epsf_target_angle;

  Byte to_set(data + 4);
  to_set.set_value(x, 0, 8);
}


Vcuepsfcontrolrequest469* Vcuepsfcontrolrequest469::set_high_vcu_req_epsf_target_angle(
    int high_vcu_req_epsf_target_angle) {
  high_vcu_req_epsf_target_angle_ = high_vcu_req_epsf_target_angle;
  return this;
 }

// config detail: {'bit': 24, 'is_signed_var': False, 'len': 8, 'name': 'High_VCU_Req_EPSF_Target_Angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'deg', 'precision': 1.0, 'type': 'int'}
void Vcuepsfcontrolrequest469::set_p_high_vcu_req_epsf_target_angle(uint8_t* data,
    int high_vcu_req_epsf_target_angle) {
  high_vcu_req_epsf_target_angle = ProtocolData::BoundedValue(0, 255, high_vcu_req_epsf_target_angle);
  int x = high_vcu_req_epsf_target_angle;

  Byte to_set(data + 3);
  to_set.set_value(x, 0, 8);
}


Vcuepsfcontrolrequest469* Vcuepsfcontrolrequest469::set_vcu_control_epsf_reserved_1(
    int vcu_control_epsf_reserved_1) {
  vcu_control_epsf_reserved_1_ = vcu_control_epsf_reserved_1;
  return this;
 }

// config detail: {'bit': 8, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Control_EPSF_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuepsfcontrolrequest469::set_p_vcu_control_epsf_reserved_1(uint8_t* data,
    int vcu_control_epsf_reserved_1) {
  vcu_control_epsf_reserved_1 = ProtocolData::BoundedValue(0, 255, vcu_control_epsf_reserved_1);
  int x = vcu_control_epsf_reserved_1;

  Byte to_set(data + 1);
  to_set.set_value(x, 0, 8);
}


Vcuepsfcontrolrequest469* Vcuepsfcontrolrequest469::set_vcu_request_epsf_control_mode(
    int vcu_request_epsf_control_mode) {
  vcu_request_epsf_control_mode_ = vcu_request_epsf_control_mode;
  return this;
 }

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Control_Mode', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcuepsfcontrolrequest469::set_p_vcu_request_epsf_control_mode(uint8_t* data,
    int vcu_request_epsf_control_mode) {
  vcu_request_epsf_control_mode = ProtocolData::BoundedValue(0, 255, vcu_request_epsf_control_mode);
  int x = vcu_request_epsf_control_mode;

  Byte to_set(data + 0);
  to_set.set_value(x, 0, 8);
}

}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
