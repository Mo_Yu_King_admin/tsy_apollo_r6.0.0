/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/tsy/tsy_message_manager.h"

#include "modules/canbus/vehicle/tsy/protocol/vcu_dbsf_request_154.h"
#include "modules/canbus/vehicle/tsy/protocol/vcu_epsf_control_request_469.h"
#include "modules/canbus/vehicle/tsy/protocol/vcu_epsr_control_request_101.h"
#include "modules/canbus/vehicle/tsy/protocol/vcu_vehicle_diagnosis_301.h"
#include "modules/canbus/vehicle/tsy/protocol/vcu_vehicle_status_1_303.h"

#include "modules/canbus/vehicle/tsy/protocol/dbsf_status_142.h"
#include "modules/canbus/vehicle/tsy/protocol/epb_status_375_375.h"
#include "modules/canbus/vehicle/tsy/protocol/epsf_status_401.h"
#include "modules/canbus/vehicle/tsy/protocol/epsr_status_111.h"
#include "modules/canbus/vehicle/tsy/protocol/iecu_control_distributed_505.h"
#include "modules/canbus/vehicle/tsy/protocol/iecu_control_flag_501.h"
#include "modules/canbus/vehicle/tsy/protocol/iecu_control_ibc_503.h"
#include "modules/canbus/vehicle/tsy/protocol/iecu_control_power_504.h"
#include "modules/canbus/vehicle/tsy/protocol/iecu_control_steering_502.h"
#include "modules/canbus/vehicle/tsy/protocol/imu_acceleration_181.h"
#include "modules/canbus/vehicle/tsy/protocol/imu_angular_281.h"
#include "modules/canbus/vehicle/tsy/protocol/imu_euler_381.h"
#include "modules/canbus/vehicle/tsy/protocol/remote_control_io_10a.h"
#include "modules/canbus/vehicle/tsy/protocol/remote_control_shake_2_10b.h"
#include "modules/canbus/vehicle/tsy/protocol/vcu_epb_request_310_310.h"
#include "modules/canbus/vehicle/tsy/protocol/vcu_vehicle_status_2_304.h"

namespace apollo {
namespace canbus {
namespace tsy {

TsyMessageManager::TsyMessageManager() {
  // Control Messages
  AddSendProtocolData<Vcudbsfrequest154, true>();
  AddSendProtocolData<Vcuepsfcontrolrequest469, true>();
  AddSendProtocolData<Vcuepsrcontrolrequest101, true>();
  AddSendProtocolData<Vcuvehiclediagnosis301, true>();
  AddSendProtocolData<Vcuvehiclestatus1303, true>();

  // Report Messages
  AddRecvProtocolData<Dbsfstatus142, true>();
  AddRecvProtocolData<Epbstatus375375, true>();
  AddRecvProtocolData<Epsfstatus401, true>();
  AddRecvProtocolData<Epsrstatus111, true>();
  AddRecvProtocolData<Iecucontroldistributed505, true>();
  AddRecvProtocolData<Iecucontrolflag501, true>();
  AddRecvProtocolData<Iecucontrolibc503, true>();
  AddRecvProtocolData<Iecucontrolpower504, true>();
  AddRecvProtocolData<Iecucontrolsteering502, true>();
  AddRecvProtocolData<Imuacceleration181, true>();
  AddRecvProtocolData<Imuangular281, true>();
  AddRecvProtocolData<Imueuler381, true>();
  AddRecvProtocolData<Remotecontrolio10a, true>();
  AddRecvProtocolData<Remotecontrolshake210b, true>();
  AddRecvProtocolData<Vcuepbrequest310310, true>();
  AddRecvProtocolData<Vcuvehiclestatus2304, true>();
}

TsyMessageManager::~TsyMessageManager() {}

}  // namespace tsy
}  // namespace canbus
}  // namespace apollo
