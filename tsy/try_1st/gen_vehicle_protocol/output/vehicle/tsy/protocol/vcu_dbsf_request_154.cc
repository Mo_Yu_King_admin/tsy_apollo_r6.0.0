/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/vcu_dbsf_request_154.h"

#include "modules/drivers/canbus/common/byte.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

const int32_t Vcudbsfrequest154::ID = 0x154;

// public
Vcudbsfrequest154::Vcudbsfrequest154() { Reset(); }

uint32_t Vcudbsfrequest154::GetPeriod() const {
  // TODO(All) :  modify every protocol's period manually
  static const uint32_t PERIOD = 20 * 1000;
  return PERIOD;
}

void Vcudbsfrequest154::UpdateData(uint8_t* data) {
  set_p_vcu_dbsf_reserved_3(data, vcu_dbsf_reserved_3_);
  set_p_vcu_dbsf_reserved_2(data, vcu_dbsf_reserved_2_);
  set_p_vcu_dbsf_pressure_request(data, vcu_dbsf_pressure_request_);
  set_p_vcu_dbsf_request_flag(data, vcu_dbsf_request_flag_);
  set_p_vcu_dbsf_reserved_1(data, vcu_dbsf_reserved_1_);
}

void Vcudbsfrequest154::Reset() {
  // TODO(All) :  you should check this manually
  vcu_dbsf_reserved_3_ = 0;
  vcu_dbsf_reserved_2_ = 0;
  vcu_dbsf_pressure_request_ = 0.0;
  vcu_dbsf_request_flag_ = false;
  vcu_dbsf_reserved_1_ = 0;
}

Vcudbsfrequest154* Vcudbsfrequest154::set_vcu_dbsf_reserved_3(
    int vcu_dbsf_reserved_3) {
  vcu_dbsf_reserved_3_ = vcu_dbsf_reserved_3;
  return this;
 }

// config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'VCU_DBSF_Reserved_3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcudbsfrequest154::set_p_vcu_dbsf_reserved_3(uint8_t* data,
    int vcu_dbsf_reserved_3) {
  vcu_dbsf_reserved_3 = ProtocolData::BoundedValue(0, 255, vcu_dbsf_reserved_3);
  int x = vcu_dbsf_reserved_3;

  Byte to_set(data + 7);
  to_set.set_value(x, 0, 8);
}


Vcudbsfrequest154* Vcudbsfrequest154::set_vcu_dbsf_reserved_2(
    int vcu_dbsf_reserved_2) {
  vcu_dbsf_reserved_2_ = vcu_dbsf_reserved_2;
  return this;
 }

// config detail: {'bit': 18, 'is_signed_var': False, 'len': 22, 'name': 'VCU_DBSF_Reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcudbsfrequest154::set_p_vcu_dbsf_reserved_2(uint8_t* data,
    int vcu_dbsf_reserved_2) {
  vcu_dbsf_reserved_2 = ProtocolData::BoundedValue(0, 100, vcu_dbsf_reserved_2);
  int x = vcu_dbsf_reserved_2;
  uint8_t t = 0;

  t = x & 0x3F;
  Byte to_set0(data + 2);
  to_set0.set_value(t, 2, 6);
  x >>= 6;

  t = x & 0xFF;
  Byte to_set1(data + 3);
  to_set1.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set2(data + 4);
  to_set2.set_value(t, 0, 8);
}


Vcudbsfrequest154* Vcudbsfrequest154::set_vcu_dbsf_pressure_request(
    double vcu_dbsf_pressure_request) {
  vcu_dbsf_pressure_request_ = vcu_dbsf_pressure_request;
  return this;
 }

// config detail: {'bit': 40, 'is_signed_var': False, 'len': 16, 'name': 'VCU_DBSF_Pressure_Request', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|8]', 'physical_unit': 'Mpa', 'precision': 0.016, 'type': 'double'}
void Vcudbsfrequest154::set_p_vcu_dbsf_pressure_request(uint8_t* data,
    double vcu_dbsf_pressure_request) {
  vcu_dbsf_pressure_request = ProtocolData::BoundedValue(0.0, 8.0, vcu_dbsf_pressure_request);
  int x = vcu_dbsf_pressure_request / 0.016000;
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 5);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 6);
  to_set1.set_value(t, 0, 8);
}


Vcudbsfrequest154* Vcudbsfrequest154::set_vcu_dbsf_request_flag(
    bool vcu_dbsf_request_flag) {
  vcu_dbsf_request_flag_ = vcu_dbsf_request_flag;
  return this;
 }

// config detail: {'bit': 17, 'is_signed_var': False, 'len': 1, 'name': 'VCU_DBSF_Request_Flag', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
void Vcudbsfrequest154::set_p_vcu_dbsf_request_flag(uint8_t* data,
    bool vcu_dbsf_request_flag) {
  int x = vcu_dbsf_request_flag;

  Byte to_set(data + 2);
  to_set.set_value(x, 1, 1);
}


Vcudbsfrequest154* Vcudbsfrequest154::set_vcu_dbsf_reserved_1(
    int vcu_dbsf_reserved_1) {
  vcu_dbsf_reserved_1_ = vcu_dbsf_reserved_1;
  return this;
 }

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 17, 'name': 'VCU_DBSF_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|500]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
void Vcudbsfrequest154::set_p_vcu_dbsf_reserved_1(uint8_t* data,
    int vcu_dbsf_reserved_1) {
  vcu_dbsf_reserved_1 = ProtocolData::BoundedValue(0, 500, vcu_dbsf_reserved_1);
  int x = vcu_dbsf_reserved_1;
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 0);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 1);
  to_set1.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0x1;
  Byte to_set2(data + 2);
  to_set2.set_value(t, 0, 1);
}

}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
