/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace TSY {

class Vcuepsfcontrolrequest469 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;

  Vcuepsfcontrolrequest469();

  uint32_t GetPeriod() const override;

  void UpdateData(uint8_t* data) override;

  void Reset() override;

  // config detail: {'bit': 16, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Control_EPSF_Reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuepsfcontrolrequest469* set_vcu_control_epsf_reserved_2(int vcu_control_epsf_reserved_2);

  // config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'VCU_EPSF_CheckSum', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuepsfcontrolrequest469* set_vcu_epsf_checksum(int vcu_epsf_checksum);

  // config detail: {'bit': 48, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Angle_Speed', 'offset': 0.0, 'order': 'intel', 'physical_range': '[20|250]', 'physical_unit': 'deg/s', 'precision': 1.0, 'type': 'int'}
  Vcuepsfcontrolrequest469* set_vcu_request_epsf_angle_speed(int vcu_request_epsf_angle_speed);

  // config detail: {'bit': 40, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Angle_Calibrate', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuepsfcontrolrequest469* set_vcu_request_epsf_angle_calibrate(int vcu_request_epsf_angle_calibrate);

  // config detail: {'bit': 32, 'is_signed_var': False, 'len': 8, 'name': 'Low_VCU_Req_EPSF_Target_Angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'deg', 'precision': 1.0, 'type': 'int'}
  Vcuepsfcontrolrequest469* set_low_vcu_req_epsf_target_angle(int low_vcu_req_epsf_target_angle);

  // config detail: {'bit': 24, 'is_signed_var': False, 'len': 8, 'name': 'High_VCU_Req_EPSF_Target_Angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'deg', 'precision': 1.0, 'type': 'int'}
  Vcuepsfcontrolrequest469* set_high_vcu_req_epsf_target_angle(int high_vcu_req_epsf_target_angle);

  // config detail: {'bit': 8, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Control_EPSF_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuepsfcontrolrequest469* set_vcu_control_epsf_reserved_1(int vcu_control_epsf_reserved_1);

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Control_Mode', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuepsfcontrolrequest469* set_vcu_request_epsf_control_mode(int vcu_request_epsf_control_mode);

 private:

  // config detail: {'bit': 16, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Control_EPSF_Reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_control_epsf_reserved_2(uint8_t* data, int vcu_control_epsf_reserved_2);

  // config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'VCU_EPSF_CheckSum', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_epsf_checksum(uint8_t* data, int vcu_epsf_checksum);

  // config detail: {'bit': 48, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Angle_Speed', 'offset': 0.0, 'order': 'intel', 'physical_range': '[20|250]', 'physical_unit': 'deg/s', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_request_epsf_angle_speed(uint8_t* data, int vcu_request_epsf_angle_speed);

  // config detail: {'bit': 40, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Angle_Calibrate', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_request_epsf_angle_calibrate(uint8_t* data, int vcu_request_epsf_angle_calibrate);

  // config detail: {'bit': 32, 'is_signed_var': False, 'len': 8, 'name': 'Low_VCU_Req_EPSF_Target_Angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'deg', 'precision': 1.0, 'type': 'int'}
  void set_p_low_vcu_req_epsf_target_angle(uint8_t* data, int low_vcu_req_epsf_target_angle);

  // config detail: {'bit': 24, 'is_signed_var': False, 'len': 8, 'name': 'High_VCU_Req_EPSF_Target_Angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'deg', 'precision': 1.0, 'type': 'int'}
  void set_p_high_vcu_req_epsf_target_angle(uint8_t* data, int high_vcu_req_epsf_target_angle);

  // config detail: {'bit': 8, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Control_EPSF_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_control_epsf_reserved_1(uint8_t* data, int vcu_control_epsf_reserved_1);

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Request_EPSF_Control_Mode', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_request_epsf_control_mode(uint8_t* data, int vcu_request_epsf_control_mode);

 private:
  int vcu_control_epsf_reserved_2_;
  int vcu_epsf_checksum_;
  int vcu_request_epsf_angle_speed_;
  int vcu_request_epsf_angle_calibrate_;
  int low_vcu_req_epsf_target_angle_;
  int high_vcu_req_epsf_target_angle_;
  int vcu_control_epsf_reserved_1_;
  int vcu_request_epsf_control_mode_;
};

}  // namespace TSY
}  // namespace canbus
}  // namespace apollo


