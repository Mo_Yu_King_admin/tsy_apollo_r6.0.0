/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace TSY {

class Vcuvehiclestatus1303 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;

  Vcuvehiclestatus1303();

  uint32_t GetPeriod() const override;

  void UpdateData(uint8_t* data) override;

  void Reset() override;

  // config detail: {'bit': 24, 'is_signed_var': False, 'len': 8, 'name': 'EPB_Status_FeedBack', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclestatus1303* set_epb_status_feedback(int epb_status_feedback);

  // config detail: {'bit': 48, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Status_Reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclestatus1303* set_vehicle_status_reserved(int vehicle_status_reserved);

  // config detail: {'bit': 32, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Voltage', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'V', 'precision': 0.1, 'type': 'double'}
  Vcuvehiclestatus1303* set_vehicle_voltage(double vehicle_voltage);

  // config detail: {'bit': 16, 'is_signed_var': False, 'len': 8, 'name': 'BMS_Status', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclestatus1303* set_bms_status(int bms_status);

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Ready_Flag', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclestatus1303* set_vcu_ready_flag(int vcu_ready_flag);

  // config detail: {'bit': 8, 'is_signed_var': False, 'len': 8, 'name': 'IgnitionStatus', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  Vcuvehiclestatus1303* set_ignitionstatus(int ignitionstatus);

 private:

  // config detail: {'bit': 24, 'is_signed_var': False, 'len': 8, 'name': 'EPB_Status_FeedBack', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_epb_status_feedback(uint8_t* data, int epb_status_feedback);

  // config detail: {'bit': 48, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Status_Reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vehicle_status_reserved(uint8_t* data, int vehicle_status_reserved);

  // config detail: {'bit': 32, 'is_signed_var': False, 'len': 16, 'name': 'Vehicle_Voltage', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'V', 'precision': 0.1, 'type': 'double'}
  void set_p_vehicle_voltage(uint8_t* data, double vehicle_voltage);

  // config detail: {'bit': 16, 'is_signed_var': False, 'len': 8, 'name': 'BMS_Status', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_bms_status(uint8_t* data, int bms_status);

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'VCU_Ready_Flag', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_vcu_ready_flag(uint8_t* data, int vcu_ready_flag);

  // config detail: {'bit': 8, 'is_signed_var': False, 'len': 8, 'name': 'IgnitionStatus', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  void set_p_ignitionstatus(uint8_t* data, int ignitionstatus);

 private:
  int epb_status_feedback_;
  int vehicle_status_reserved_;
  double vehicle_voltage_;
  int bms_status_;
  int vcu_ready_flag_;
  int ignitionstatus_;
};

}  // namespace TSY
}  // namespace canbus
}  // namespace apollo


