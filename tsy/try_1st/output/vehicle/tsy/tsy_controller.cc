/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/tsy/tsy_controller.h"

#include "modules/common/proto/vehicle_signal.pb.h"

#include "cyber/common/log.h"
#include "modules/canbus/vehicle/tsy/tsy_message_manager.h"
#include "modules/canbus/vehicle/vehicle_controller.h"
#include "modules/common/time/time.h"
#include "modules/drivers/canbus/can_comm/can_sender.h"
#include "modules/drivers/canbus/can_comm/protocol_data.h"

namespace apollo {
namespace canbus {
namespace tsy {

using ::apollo::drivers::canbus::ProtocolData;
using ::apollo::common::ErrorCode;
using ::apollo::control::ControlCommand;

namespace {

const int32_t kMaxFailAttempt = 10;
const int32_t CHECK_RESPONSE_STEER_UNIT_FLAG = 1;
const int32_t CHECK_RESPONSE_SPEED_UNIT_FLAG = 2;
}

ErrorCode TsyController::Init(
	const VehicleParameter& params,
	CanSender<::apollo::canbus::ChassisDetail> *const can_sender,
    MessageManager<::apollo::canbus::ChassisDetail> *const message_manager) {
  if (is_initialized_) {
    AINFO << "TsyController has already been initiated.";
    return ErrorCode::CANBUS_ERROR;
  }

  params_.CopyFrom(params);
  if (!params_.has_driving_mode()) {
    AERROR << "Vehicle conf pb not set driving_mode.";
    return ErrorCode::CANBUS_ERROR;
  }

  if (can_sender == nullptr) {
    return ErrorCode::CANBUS_ERROR;
  }
  can_sender_ = can_sender;

  if (message_manager == nullptr) {
    AERROR << "protocol manager is null.";
    return ErrorCode::CANBUS_ERROR;
  }
  message_manager_ = message_manager;

  // sender part
  vcu_dbsf_request_154_ = dynamic_cast<Vcudbsfrequest154*>
          (message_manager_->GetMutableProtocolDataById(Vcudbsfrequest154::ID));
  if (vcu_dbsf_request_154_ == nullptr) {
     AERROR << "Vcudbsfrequest154 does not exist in the TsyMessageManager!";
     return ErrorCode::CANBUS_ERROR;
  }

  vcu_epsf_control_request_469_ = dynamic_cast<Vcuepsfcontrolrequest469*>
          (message_manager_->GetMutableProtocolDataById(Vcuepsfcontrolrequest469::ID));
  if (vcu_epsf_control_request_469_ == nullptr) {
     AERROR << "Vcuepsfcontrolrequest469 does not exist in the TsyMessageManager!";
     return ErrorCode::CANBUS_ERROR;
  }

  vcu_epsr_control_request_101_ = dynamic_cast<Vcuepsrcontrolrequest101*>
          (message_manager_->GetMutableProtocolDataById(Vcuepsrcontrolrequest101::ID));
  if (vcu_epsr_control_request_101_ == nullptr) {
     AERROR << "Vcuepsrcontrolrequest101 does not exist in the TsyMessageManager!";
     return ErrorCode::CANBUS_ERROR;
  }

  vcu_vehicle_diagnosis_301_ = dynamic_cast<Vcuvehiclediagnosis301*>
          (message_manager_->GetMutableProtocolDataById(Vcuvehiclediagnosis301::ID));
  if (vcu_vehicle_diagnosis_301_ == nullptr) {
     AERROR << "Vcuvehiclediagnosis301 does not exist in the TsyMessageManager!";
     return ErrorCode::CANBUS_ERROR;
  }

  vcu_vehicle_status_1_303_ = dynamic_cast<Vcuvehiclestatus1303*>
          (message_manager_->GetMutableProtocolDataById(Vcuvehiclestatus1303::ID));
  if (vcu_vehicle_status_1_303_ == nullptr) {
     AERROR << "Vcuvehiclestatus1303 does not exist in the TsyMessageManager!";
     return ErrorCode::CANBUS_ERROR;
  }

  can_sender_->AddMessage(Vcudbsfrequest154::ID, vcu_dbsf_request_154_, false);
  can_sender_->AddMessage(Vcuepsfcontrolrequest469::ID, vcu_epsf_control_request_469_, false);
  can_sender_->AddMessage(Vcuepsrcontrolrequest101::ID, vcu_epsr_control_request_101_, false);
  can_sender_->AddMessage(Vcuvehiclediagnosis301::ID, vcu_vehicle_diagnosis_301_, false);
  can_sender_->AddMessage(Vcuvehiclestatus1303::ID, vcu_vehicle_status_1_303_, false);

  // need sleep to ensure all messages received
  AINFO << "TsyController is initialized.";

  is_initialized_ = true;
  return ErrorCode::OK;
}

TsyController::~TsyController() {}

bool TsyController::Start() {
  if (!is_initialized_) {
    AERROR << "TsyController has NOT been initiated.";
    return false;
  }
  const auto& update_func = [this] { SecurityDogThreadFunc(); };
  thread_.reset(new std::thread(update_func));

  return true;
}

void TsyController::Stop() {
  if (!is_initialized_) {
    AERROR << "TsyController stops or starts improperly!";
    return;
  }

  if (thread_ != nullptr && thread_->joinable()) {
    thread_->join();
    thread_.reset();
    AINFO << "TsyController stopped.";
  }
}

Chassis TsyController::chassis() {
  chassis_.Clear();

  ChassisDetail chassis_detail;
  message_manager_->GetSensorData(&chassis_detail);

  // 21, 22, previously 1, 2
  if (driving_mode() == Chassis::EMERGENCY_MODE) {
    set_chassis_error_code(Chassis::NO_ERROR);
  }

  chassis_.set_driving_mode(driving_mode());
  chassis_.set_error_code(chassis_error_code());

  // 3
  chassis_.set_engine_started(true);
  /* ADD YOUR OWN CAR CHASSIS OPERATION
  */
  if (!chassis_detail.has_tsy()) {
    AERROR << "NO TSY chassis information!";
    return chassis_;
  }
  TSY tsy = chassis_detail.tsy();
  //4 Tsy:tsy 没有发动机转速信号
  //5 wheel spd
  if (tsy.has_mcufl_torque_feedback_10()) {
    if (tsy.mcufl_torque_feedback_10().has_fl_speed()) {
      chassis_.mutable_wheel_speed()->set_wheel_spd_fl(
          tsy.mcufl_torque_feedback_10().fl_speed());
    }
  }
  if (tsy.has_mcufr_torque_feedback_20()) {
    if (tsy.mcufr_torque_feedback_20().has_fr_speed()) {
      chassis_.mutable_wheel_speed()->set_wheel_spd_fr(
          tsy.mcufr_torque_feedback_20().fr_speed());
    }
  }
  if (tsy.has_mcurl_torque_feedback_30()) {
    if (tsy.mcurl_torque_feedback_30().has_rl_speed()) {
      chassis_.mutable_wheel_speed()->set_wheel_spd_rl(
          tsy.mcurl_torque_feedback_30().rl_speed());
    }
  }
  if (tsy.has_mcurr_torque_feedback_40()) {
    if (tsy.mcurr_torque_feedback_40().has_rr_speed()) {
      chassis_.mutable_wheel_speed()->set_wheel_spd_rr(
          tsy.mcurr_torque_feedback_40().rr_speed());
    }
  }
  //6 speed_mps 这个是米每秒的车速
  if (tsy.has_vcu_vehicle_status_2_304() && tsy.vcu_vehicle_status_2_304().has_vehicle_speed()) {
    chassis_.set_speed_mps(
        static_cast<float>(tsy.vcu_vehicle_status_2_304().vehicle_speed()));
  } else {
    chassis_.set_speed_mps(0);
  }
  // 7 no odometer
  // chassis_.set_odometer_m(0);
  // 8 no fuel. do not set;
  // chassis_.set_fuel_range_m(0);
  // 9 throttle 这里有一个问题，这是油门的实际反馈，我们缺少这部分的DBC和定义
  /*
  if (tsy().has_throttle_report_500() &&
      tsy().throttle_report_500().has_throttle_pedal_actual()) {
    chassis_.set_throttle_percentage(static_cast<float>(
        tsy().throttle_report_500().throttle_pedal_actual()));
  } else {
    chassis_.set_throttle_percentage(0);
  }
  */
  // 10 brake 这里也有一个问题，要求的是制动百分比的反馈，所以在制动压力反馈的DBC
  //          中应该除以一下最大的制动压力
  if (tsy.has_dbsf_status_142() &&
      tsy.dbsf_status_142().has_dbsf_hp_pressure()) {
    chassis_.set_brake_percentage(static_cast<float>(
        tsy.dbsf_status_142().has_dbsf_hp_pressure()));
  } else {
    chassis_.set_brake_percentage(0);
  }
  // 23, previously 11 gear
  /*
  if (tsy.has_gear_report_303() &&
      tsy.gear_report_503().has_gear_actual()) {
    Chassis::GearPosition gear_pos = Chassis::GEAR_INVALID;

    if (tsy.gear_report_503().gear_actual() ==
        Gear_report_503::GEAR_ACTUAL_INVALID) {
      gear_pos = Chassis::GEAR_INVALID;
    }
    if (tsy.gear_report_503().gear_actual() ==
        Gear_report_503::GEAR_ACTUAL_NEUTRAL) {
      gear_pos = Chassis::GEAR_NEUTRAL;
    }
    if (tsy.gear_report_503().gear_actual() ==
        Gear_report_503::GEAR_ACTUAL_REVERSE) {
      gear_pos = Chassis::GEAR_REVERSE;
    }
    if (tsy.gear_report_503().gear_actual() ==
        Gear_report_503::GEAR_ACTUAL_DRIVE) {
      gear_pos = Chassis::GEAR_DRIVE;
    }
    if (tsy.gear_report_503().gear_actual() ==
        Gear_report_503::GEAR_ACTUAL_PARK) {
      gear_pos = Chassis::GEAR_PARKING;
    }
    chassis_.set_gear_location(gear_pos);
  } else {
    chassis_.set_gear_location(Chassis::GEAR_NONE);
  }
  */
  // 12 steering
  if (tsy.has_vcu_vehicle_status_2_304() &&
      tsy.vcu_vehicle_status_2_304().has_f_tire_angle()) {
    chassis_.set_steering_percentage(static_cast<float>(
        tsy.vcu_vehicle_status_2_304().f_tire_angle() *
        100.0 / vehicle_params_.max_steer_angle() * M_PI / 180));
  } else {
    chassis_.set_steering_percentage(0);
  }
  // 13 parking brake
  if (tsy.has_vcu_vehicle_status_1_303() &&
      tsy.vcu_vehicle_status_1_303().has_epb_final_state()) {
    if (tsy.vcu_vehicle_status_1_303().epb_final_state() ==
        Vcu_vehicle_status_1_303::PARKED) {
      chassis_.set_parking_brake(true);
    } else {
      chassis_.set_parking_brake(false);
    }
  } else {
    chassis_.set_parking_brake(false);
  }

  return chassis_;
}

void TsyController::Emergency() {
  set_driving_mode(Chassis::EMERGENCY_MODE);
  ResetProtocol();
}

ErrorCode TsyController::EnableAutoMode() {
  if (driving_mode() == Chassis::COMPLETE_AUTO_DRIVE) {
    AINFO << "already in COMPLETE_AUTO_DRIVE mode";
    return ErrorCode::OK;
  }
  // set enable
  iecu_control_flag_501_->set_iecu_control_request_flag(
      Iecu_control_flag_501::ENABLE)
  iecu_control_steering_502_->set_iecu_steering_valid(
      Iecu_control_steering_502::ENABLE);
  iecu_control_ibc_503_->set_iecu_ibc_valid(
      Iecu_control_ibc_503::ENABLE);
  iecu_control_power_504_->set_iecu_power_valid(
      Iecu_control_power_504::ENABLE);
  gear_command_103_->set_gear_en_ctrl(Gear_command_103::GEAR_EN_CTRL_ENABLE);
  park_command_104_->set_park_en_ctrl(Park_command_104::PARK_EN_CTRL_ENABLE);

  can_sender_->Update();
  const int32_t flag =
      CHECK_RESPONSE_STEER_UNIT_FLAG | CHECK_RESPONSE_SPEED_UNIT_FLAG;
  if (!CheckResponse(flag, true)) {
    AERROR << "Failed to switch to COMPLETE_AUTO_DRIVE mode.";
    Emergency();
    set_chassis_error_code(Chassis::CHASSIS_ERROR);
    return ErrorCode::CANBUS_ERROR;
  }
  set_driving_mode(Chassis::COMPLETE_AUTO_DRIVE);
  AINFO << "Switch to COMPLETE_AUTO_DRIVE mode ok.";

  return ErrorCode::OK;
}

ErrorCode TsyController::DisableAutoMode() {
  ResetProtocol();
  can_sender_->Update();
  set_driving_mode(Chassis::COMPLETE_MANUAL);
  set_chassis_error_code(Chassis::NO_ERROR);
  AINFO << "Switch to COMPLETE_MANUAL ok.";
  return ErrorCode::OK;
}

ErrorCode TsyController::EnableSteeringOnlyMode() {
  if (driving_mode() == Chassis::COMPLETE_AUTO_DRIVE ||
      driving_mode() == Chassis::AUTO_STEER_ONLY) {
    set_driving_mode(Chassis::AUTO_STEER_ONLY);
    AINFO << "Already in AUTO_STEER_ONLY mode.";
    return ErrorCode::OK;
  }
  AFATAL << "SteerOnlyMode is not supported in this car!";
  return ErrorCode::CANBUS_ERROR;
  /* ADD YOUR OWN CAR CHASSIS OPERATION
  brake_60_->set_disable();
  throttle_62_->set_disable();
  steering_64_->set_enable();

  can_sender_->Update();
  if (!CheckResponse(CHECK_RESPONSE_STEER_UNIT_FLAG, true)) {
    AERROR << "Failed to switch to AUTO_STEER_ONLY mode.";
    Emergency();
    set_chassis_error_code(Chassis::CHASSIS_ERROR);
    return ErrorCode::CANBUS_ERROR;
  }
  set_driving_mode(Chassis::AUTO_STEER_ONLY);
  AINFO << "Switch to AUTO_STEER_ONLY mode ok.";
  return ErrorCode::OK;
  */
}

ErrorCode TsyController::EnableSpeedOnlyMode() {
  if (driving_mode() == Chassis::COMPLETE_AUTO_DRIVE ||
      driving_mode() == Chassis::AUTO_SPEED_ONLY) {
    set_driving_mode(Chassis::AUTO_SPEED_ONLY);
    AINFO << "Already in AUTO_SPEED_ONLY mode";
    return ErrorCode::OK;
  }
  AFATAL << "SpeedOnlyMode is not supported in this car!";
  return ErrorCode::CANBUS_ERROR;
  /* ADD YOUR OWN CAR CHASSIS OPERATION
  brake_60_->set_enable();
  throttle_62_->set_enable();
  steering_64_->set_disable();

  can_sender_->Update();
  if (!CheckResponse(CHECK_RESPONSE_SPEED_UNIT_FLAG, true)) {
    AERROR << "Failed to switch to AUTO_SPEED_ONLY mode.";
    Emergency();
    set_chassis_error_code(Chassis::CHASSIS_ERROR);
    return ErrorCode::CANBUS_ERROR;
  }
  set_driving_mode(Chassis::AUTO_SPEED_ONLY);
  AINFO << "Switch to AUTO_SPEED_ONLY mode ok.";
  return ErrorCode::OK;
  */
}

// NEUTRAL, REVERSE, DRIVE
void TsyController::Gear(Chassis::GearPosition gear_position) {
  if (driving_mode() != Chassis::COMPLETE_AUTO_DRIVE &&
      driving_mode() != Chassis::AUTO_SPEED_ONLY) {
    AINFO << "This drive mode no need to set gear.";
    return;
  }
  // ADD YOUR OWN CAR CHASSIS OPERATION
  switch (gear_position) {
    case Chassis::GEAR_NEUTRAL: {
      iecu_control_power_504_->set_iecu_power_gear(Iecu_control_power_504::NEUTRAL);
      break;
    }
    case Chassis::GEAR_REVERSE: {
      iecu_control_power_504_->set_iecu_power_gear(Iecu_control_power_504::REVERSE);
      break;
    }
    case Chassis::GEAR_DRIVE: {
      iecu_control_power_504_->set_iecu_power_gear(Iecu_control_power_504::DRIVE);
      break;
    }
    case Chassis::GEAR_PARKING: {
      iecu_control_power_504_->set_iecu_power_gear(Iecu_control_power_504::PARKING);
      break;
    }
    /*
    case Chassis::GEAR_LOW: {
      gear_66_->set_gear_low();
      break;
    }
    并没有设计低速档，所以这段不用。
    */
    case Chassis::GEAR_NONE: {
      iecu_control_power_504_->set_iecu_power_gear(Iecu_control_power_504::NEUTRAL);
      break;
    }
    case Chassis::GEAR_INVALID: {
      AERROR << "Gear command is invalid!";
      iecu_control_power_504_->set_iecu_power_gear(Iecu_control_power_504::NEUTRAL);
      break;
    }
    default: {
      iecu_control_power_504_->set_iecu_power_gear(Iecu_control_power_504::NEUTRAL);
      break;
    }
  }
}

// brake with new acceleration
// acceleration:0.00~99.99, unit:
// acceleration:0.0 ~ 7.0, unit:m/s^2
// acceleration_spd:60 ~ 100, suggest: 90
// -> pedal
void TsyController::Brake(double pedal) {
  // double real_value = params_.max_acc() * acceleration / 100;
  // TODO(All) :  Update brake value based on mode
  if (driving_mode() != Chassis::COMPLETE_AUTO_DRIVE &&
      driving_mode() != Chassis::AUTO_SPEED_ONLY) {
    AINFO << "The current drive mode does not need to set brake pedal.";
    return;
  }
  iecu_control_ibc_503_->set_iecu_breakpressure_cmd(pedal);
}

// drive with old acceleration
// gas:0.00~99.99 unit:
void TsyController::Throttle(double pedal) {
  if (driving_mode() != Chassis::COMPLETE_AUTO_DRIVE &&
      driving_mode() != Chassis::AUTO_SPEED_ONLY) {
    AINFO << "The current drive mode does not need to set throttle pedal.";
    return;
  }
  iecu_control_power_504_->set_iecu_total_or_distribute(
      Iecu_control_power_504::TOTAL);
  iecu_control_power_504_->set_torque_or_speed_or_acc(
      Iecu_control_power_504::TORQUE);
  iecu_control_power_504_->set_iecu_torque_control(pedal);
}

// confirm the car is driven by acceleration command or throttle/brake pedal
// drive with acceleration/deceleration
// acc:-7.0 ~ 5.0, unit:m/s^2
void TsyController::Acceleration(double acc) {
  if (driving_mode() != Chassis::COMPLETE_AUTO_DRIVE ||
      driving_mode() != Chassis::AUTO_SPEED_ONLY) {
    AINFO << "The current drive mode does not need to set acceleration.";
    return;
  }
  iecu_control_power_504_->set_iecu_total_or_distribute(
      Iecu_control_power_504::TOTAL);
  iecu_control_power_504_->set_torque_or_speed_or_acc(
      Iecu_control_power_504::ACC);
  iecu_control_power_504_->set_iecu_acc_or_de_control(acc);
}

// tsy default, -470 ~ 470, left:+, right:-
// need to be compatible with control module, so reverse
// steering with old angle speed
// angle:-99.99~0.00~99.99, unit:, left:-, right:+
void TsyController::Steer(double angle) {
  if (driving_mode() != Chassis::COMPLETE_AUTO_DRIVE &&
      driving_mode() != Chassis::AUTO_STEER_ONLY) {
    AINFO << "The current driving mode does not need to set steer.";
    return;
  }
  const double real_angle = params_.max_steer_angle() * angle / 100.0;
  // reverse sign
  iecu_control_steering_502_->set_iecu_ftire_angle_cmd(real_angle);
  iecu_control_steering_502_->set_iecu_ftire_speed_cmd(200);
}

// steering with new angle speed
// angle:-99.99~0.00~99.99, unit:, left:-, right:+
// angle_spd:0.00~99.99, unit:deg/s
void TsyController::Steer(double angle, double angle_spd) {
  if (driving_mode() != Chassis::COMPLETE_AUTO_DRIVE &&
      driving_mode() != Chassis::AUTO_STEER_ONLY) {
    AINFO << "The current driving mode does not need to set steer.";
    return;
  }
  const double real_angle = params_.max_steer_angle() * angle / 100.0;
  const double real_angle_spd = ProtocolData::BoundedValue(
      params_.min_steer_angle_spd(), params_.max_steer_angle_spd(),
      params_.max_steer_angle_spd() * angle_spd / 100.0);
  // reverse sign
  iecu_control_steering_502_->set_iecu_ftire_angle_cmd(real_angle);
  iecu_control_steering_502_->set_iecu_ftire_speed_cmd(real_angle_spd);
}

void TsyController::SetEpbBreak(const ControlCommand& command) {
  if (command.parking_brake()) {
    // None
  } else {
    // None
  }
}

void TsyController::SetBeam(const ControlCommand& command) {
  if (command.signal().high_beam()) {
    // None
  } else if (command.signal().low_beam()) {
    // None
  } else {
    // None
  }
}

void TsyController::SetHorn(const ControlCommand& command) {
  if (command.signal().horn()) {
    // None
  } else {
    // None
  }
}

void TsyController::SetTurningSignal(const ControlCommand& command) {
  // Set Turn Signal
  /* ADD YOUR OWN CAR CHASSIS OPERATION
  auto signal = command.signal().turn_signal();
  if (signal == Signal::TURN_LEFT) {
    turnsignal_68_->set_turn_left();
  } else if (signal == Signal::TURN_RIGHT) {
    turnsignal_68_->set_turn_right();
  } else {
    turnsignal_68_->set_turn_none();
  }
  */
}

void TsyController::ResetProtocol() {
  message_manager_->ResetSendMessages();
}

bool TsyController::CheckChassisError() {
  ChassisDetail chassis_detail;
  message_manager_->GetSensorData(&chassis_detail);
  if (!chassis_detail.has_tsy()) {
    AERROR_EVERY(100) << "ChassisDetail has no tsy vehicle info."
                      << chassis_detail.DebugString();
    return false;
  }

  Tsy tsy = chassis_detail.tsy();

  // steer fault
  if (tsy.has_vcu_vehicle_diagnosis_301()) {
    if (Vcu_vehicle_diagnosis_301::FSTEER_FAULT ==
        tsy.vcu_vehicle_diagnosis_301().fsteering_state()) {
      return true;
    }
  }
  // drive fault
  if (tsy.has_vcu_vehicle_diagnosis_301()) {
    if (Vcu_vehicle_diagnosis_301::FL_FAULT ==
       tsy.vcu_vehicle_diagnosis_301().flmotor_state()) {
      return true;
    }
    if (Vcu_vehicle_diagnosis_301::FR_FAULT ==
       tsy.vcu_vehicle_diagnosis_301().frmotor_state()) {
      return true;
    }
    if (Vcu_vehicle_diagnosis_301::RL_FAULT ==
       tsy.vcu_vehicle_diagnosis_301().rlmotor_state()) {
      return true;
    }
    if (Vcu_vehicle_diagnosis_301::RR_FAULT ==
       tsy.vcu_vehicle_diagnosis_301().rrmotor_state()) {
      return true;
    }
  }
  // brake fault
  if (tsy.has_vcu_vehicle_diagnosis_301()) {
    if (Vcu_vehicle_diagnosis_301::BRAKE_FAULT ==
        tsy.vcu_vehicle_diagnosis_301().dbsf_state()) {
      return true;
    }
  }
  // gear fault
  /*
  挡位并没有错误码，也不知道酷黑是哪里来的这个信号。
  */
  // park fault
  if (tsy.has_vcu_vehicle_diagnosis_301()) {
    if (Vcu_vehicle_diagnosis_301::EPB_FAULT ==
        tsy.vcu_vehicle_diagnosis_301().epb_state()) {
      return true;
    }
  }
  return false;
}

void TsyController::SecurityDogThreadFunc() {
  int32_t vertical_ctrl_fail = 0;
  int32_t horizontal_ctrl_fail = 0;

  if (can_sender_ == nullptr) {
    AERROR << "Failed to run SecurityDogThreadFunc() because can_sender_ is "
              "nullptr.";
    return;
  }
  while (!can_sender_->IsRunning()) {
    std::this_thread::yield();
  }

  std::chrono::duration<double, std::micro> default_period{50000};
  int64_t start = 0;
  int64_t end = 0;
  while (can_sender_->IsRunning()) {
    start = absl::ToUnixMicros(::apollo::common::time::Clock::Now());
    const Chassis::DrivingMode mode = driving_mode();
    bool emergency_mode = false;

    // 1. horizontal control check
    if ((mode == Chassis::COMPLETE_AUTO_DRIVE ||
         mode == Chassis::AUTO_STEER_ONLY) &&
        CheckResponse(CHECK_RESPONSE_STEER_UNIT_FLAG, false) == false) {
      ++horizontal_ctrl_fail;
      if (horizontal_ctrl_fail >= kMaxFailAttempt) {
        emergency_mode = true;
        set_chassis_error_code(Chassis::MANUAL_INTERVENTION);
      }
    } else {
      horizontal_ctrl_fail = 0;
    }

    // 2. vertical control check
    if ((mode == Chassis::COMPLETE_AUTO_DRIVE ||
         mode == Chassis::AUTO_SPEED_ONLY) &&
        !CheckResponse(CHECK_RESPONSE_SPEED_UNIT_FLAG, false)) {
      ++vertical_ctrl_fail;
      if (vertical_ctrl_fail >= kMaxFailAttempt) {
        emergency_mode = true;
        set_chassis_error_code(Chassis::MANUAL_INTERVENTION);
      }
    } else {
      vertical_ctrl_fail = 0;
    }
    if (CheckChassisError()) {
      set_chassis_error_code(Chassis::CHASSIS_ERROR);
      emergency_mode = true;
    }

    if (emergency_mode && mode != Chassis::EMERGENCY_MODE) {
      set_driving_mode(Chassis::EMERGENCY_MODE);
      message_manager_->ResetSendMessages();
    }
    end = absl::ToUnixMicros(::apollo::common::time::Clock::Now());
    std::chrono::duration<double, std::micro> elapsed{end - start};
    if (elapsed < default_period) {
      std::this_thread::sleep_for(default_period - elapsed);
    } else {
      AERROR
          << "Too much time consumption in TsyController looping process:"
          << elapsed.count();
    }
  }
}

bool TsyController::CheckResponse(const int32_t flags, bool need_wait) {
  int32_t retry_num = 20;
  ChassisDetail chassis_detail;
  bool is_eps_online = false;
  bool is_vcu_online = false;
  bool is_esp_online = false;

  do {
    if (message_manager_->GetSensorData(&chassis_detail) != ErrorCode::OK) {
      AERROR_EVERY(100) << "get chassis detail failed.";
      return false;
    }
    bool check_ok = true;
    if (flags & CHECK_RESPONSE_STEER_UNIT_FLAG) {
      is_eps_online = chassis_detail.has_check_response() &&
                      chassis_detail.check_response().has_is_eps_online() &&
                      chassis_detail.check_response().is_eps_online();
      check_ok = check_ok && is_eps_online;
    }

    if (flags & CHECK_RESPONSE_SPEED_UNIT_FLAG) {
      is_vcu_online = chassis_detail.has_check_response() &&
                      chassis_detail.check_response().has_is_vcu_online() &&
                      chassis_detail.check_response().is_vcu_online();
      is_esp_online = chassis_detail.has_check_response() &&
                      chassis_detail.check_response().has_is_esp_online() &&
                      chassis_detail.check_response().is_esp_online();
      check_ok = check_ok && is_vcu_online && is_esp_online;
    }
    if (check_ok) {
      return true;
    } else {
      AINFO << "Need to check response again.";
    }
    if (need_wait) {
      --retry_num;
      std::this_thread::sleep_for(
          std::chrono::duration<double, std::milli>(20));
    }
  } while (need_wait && retry_num);

  AINFO << "check_response fail: is_eps_online:" << is_eps_online
        << ", is_vcu_online:" << is_vcu_online
        << ", is_esp_online:" << is_esp_online;
        
  return false;
}

void TsyController::set_chassis_error_mask(const int32_t mask) {
  std::lock_guard<std::mutex> lock(chassis_mask_mutex_);
  chassis_error_mask_ = mask;
}

int32_t TsyController::chassis_error_mask() {
  std::lock_guard<std::mutex> lock(chassis_mask_mutex_);
  return chassis_error_mask_;
}

Chassis::ErrorCode TsyController::chassis_error_code() {
  std::lock_guard<std::mutex> lock(chassis_error_code_mutex_);
  return chassis_error_code_;
}

void TsyController::set_chassis_error_code(
    const Chassis::ErrorCode& error_code) {
  std::lock_guard<std::mutex> lock(chassis_error_code_mutex_);
  chassis_error_code_ = error_code;
}

}  // namespace tsy
}  // namespace canbus
}  // namespace apollo
