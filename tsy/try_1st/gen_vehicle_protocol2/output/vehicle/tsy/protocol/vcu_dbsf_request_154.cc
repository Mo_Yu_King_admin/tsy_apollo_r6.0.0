/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/vcu_dbsf_request_154.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

Vcudbsfrequest154::Vcudbsfrequest154() {}
const int32_t Vcudbsfrequest154::ID = 0x154;

void Vcudbsfrequest154::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_TSY()->mutable_vcu_dbsf_request_154()->set_vcu_dbsf_reserved_3(vcu_dbsf_reserved_3(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_dbsf_request_154()->set_vcu_dbsf_reserved_2(vcu_dbsf_reserved_2(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_dbsf_request_154()->set_vcu_dbsf_pressure_request(vcu_dbsf_pressure_request(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_dbsf_request_154()->set_vcu_dbsf_request_flag(vcu_dbsf_request_flag(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_dbsf_request_154()->set_vcu_dbsf_reserved_1(vcu_dbsf_reserved_1(bytes, length));
}

// config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'vcu_dbsf_reserved_3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcudbsfrequest154::vcu_dbsf_reserved_3(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 18, 'is_signed_var': False, 'len': 22, 'name': 'vcu_dbsf_reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcudbsfrequest154::vcu_dbsf_reserved_2(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 4);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 3);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(2, 6);
  x <<= 6;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 40, 'is_signed_var': False, 'len': 16, 'name': 'vcu_dbsf_pressure_request', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|8]', 'physical_unit': 'Mpa', 'precision': 0.016, 'type': 'double'}
double Vcudbsfrequest154::vcu_dbsf_pressure_request(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 6);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 5);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.016000;
  return ret;
}

// config detail: {'bit': 17, 'is_signed_var': False, 'len': 1, 'name': 'vcu_dbsf_request_flag', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'bool'}
bool Vcudbsfrequest154::vcu_dbsf_request_flag(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(1, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 17, 'name': 'vcu_dbsf_reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|500]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcudbsfrequest154::vcu_dbsf_reserved_1(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 0);
  t = t2.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}
}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
