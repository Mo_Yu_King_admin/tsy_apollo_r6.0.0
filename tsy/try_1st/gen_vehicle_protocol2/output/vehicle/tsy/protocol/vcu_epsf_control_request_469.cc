/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/vcu_epsf_control_request_469.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

Vcuepsfcontrolrequest469::Vcuepsfcontrolrequest469() {}
const int32_t Vcuepsfcontrolrequest469::ID = 0x469;

void Vcuepsfcontrolrequest469::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_TSY()->mutable_vcu_epsf_control_request_469()->set_vcu_control_epsf_reserved_2(vcu_control_epsf_reserved_2(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_epsf_control_request_469()->set_vcu_epsf_checksum(vcu_epsf_checksum(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_epsf_control_request_469()->set_vcu_request_epsf_angle_speed(vcu_request_epsf_angle_speed(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_epsf_control_request_469()->set_vcu_request_epsf_angle_calibrate(vcu_request_epsf_angle_calibrate(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_epsf_control_request_469()->set_low_vcu_req_epsf_target_angle(low_vcu_req_epsf_target_angle(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_epsf_control_request_469()->set_high_vcu_req_epsf_target_angle(high_vcu_req_epsf_target_angle(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_epsf_control_request_469()->set_vcu_control_epsf_reserved_1(vcu_control_epsf_reserved_1(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_epsf_control_request_469()->set_vcu_request_epsf_control_mode(vcu_request_epsf_control_mode(bytes, length));
}

// config detail: {'bit': 16, 'is_signed_var': False, 'len': 8, 'name': 'vcu_control_epsf_reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuepsfcontrolrequest469::vcu_control_epsf_reserved_2(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'vcu_epsf_checksum', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuepsfcontrolrequest469::vcu_epsf_checksum(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 48, 'is_signed_var': False, 'len': 8, 'name': 'vcu_request_epsf_angle_speed', 'offset': 0.0, 'order': 'intel', 'physical_range': '[20|250]', 'physical_unit': 'deg/s', 'precision': 1.0, 'type': 'int'}
int Vcuepsfcontrolrequest469::vcu_request_epsf_angle_speed(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 6);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 40, 'is_signed_var': False, 'len': 8, 'name': 'vcu_request_epsf_angle_calibrate', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuepsfcontrolrequest469::vcu_request_epsf_angle_calibrate(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 5);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 32, 'is_signed_var': False, 'len': 8, 'name': 'low_vcu_req_epsf_target_angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'deg', 'precision': 1.0, 'type': 'int'}
int Vcuepsfcontrolrequest469::low_vcu_req_epsf_target_angle(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 4);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 24, 'is_signed_var': False, 'len': 8, 'name': 'high_vcu_req_epsf_target_angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'deg', 'precision': 1.0, 'type': 'int'}
int Vcuepsfcontrolrequest469::high_vcu_req_epsf_target_angle(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 3);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 8, 'is_signed_var': False, 'len': 8, 'name': 'vcu_control_epsf_reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuepsfcontrolrequest469::vcu_control_epsf_reserved_1(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'vcu_request_epsf_control_mode', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuepsfcontrolrequest469::vcu_request_epsf_control_mode(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}
}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
