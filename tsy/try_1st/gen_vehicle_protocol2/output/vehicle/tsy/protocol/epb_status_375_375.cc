/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/epb_status_375_375.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

Epbstatus375375::Epbstatus375375() {}
const int32_t Epbstatus375375::ID = 0x375;

void Epbstatus375375::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_TSY()->mutable_epb_status_375_375()->set_epb_status_reserved4(epb_status_reserved4(bytes, length));
  chassis->mutable_TSY()->mutable_epb_status_375_375()->set_epb_status_reserved2(epb_status_reserved2(bytes, length));
  chassis->mutable_TSY()->mutable_epb_status_375_375()->set_epb_status_reserved3(epb_status_reserved3(bytes, length));
  chassis->mutable_TSY()->mutable_epb_status_375_375()->set_epb_status_reserved1(epb_status_reserved1(bytes, length));
  chassis->mutable_TSY()->mutable_epb_status_375_375()->set_epb_imu_ax_feedback(epb_imu_ax_feedback(bytes, length));
  chassis->mutable_TSY()->mutable_epb_status_375_375()->set_epb_fault(epb_fault(bytes, length));
  chassis->mutable_TSY()->mutable_epb_status_375_375()->set_epb_manual_parking_key(epb_manual_parking_key(bytes, length));
  chassis->mutable_TSY()->mutable_epb_status_375_375()->set_epb_final_states(epb_final_states(bytes, length));
}

// config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'epb_status_reserved4', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Epbstatus375375::epb_status_reserved4(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 7, 'is_signed_var': False, 'len': 1, 'name': 'epb_status_reserved2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Epbstatus375375::epb_status_reserved2(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(7, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 24, 'is_signed_var': False, 'len': 32, 'name': 'epb_status_reserved3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Epbstatus375375::epb_status_reserved3(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 6);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 5);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 4);
  t = t2.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t3(bytes + 3);
  t = t3.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 2, 'name': 'epb_status_reserved1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Epbstatus375375::epb_status_reserved1(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 2);

  int ret = x;
  return ret;
}

// config detail: {'bit': 8, 'is_signed_var': False, 'len': 16, 'name': 'epb_imu_ax_feedback', 'offset': -1.7, 'order': 'intel', 'physical_range': '[-1.7|1.7]', 'physical_unit': 'g', 'precision': 0.0034, 'type': 'double'}
double Epbstatus375375::epb_imu_ax_feedback(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.003400 + -1.700000;
  return ret;
}

// config detail: {'bit': 4, 'enum': {0: 'EPB_FAULT_NO_FAULT', 1: 'EPB_FAULT_FAULT'}, 'is_signed_var': False, 'len': 1, 'name': 'epb_fault', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
Epb_status_375_375::Epb_faultType Epbstatus375375::epb_fault(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(4, 1);

  Epb_status_375_375::Epb_faultType ret =  static_cast<Epb_status_375_375::Epb_faultType>(x);
  return ret;
}

// config detail: {'bit': 5, 'enum': {0: 'EPB_MANUAL_PARKING_KEY_RELEASE', 1: 'EPB_MANUAL_PARKING_KEY_PULL_UP', 2: 'EPB_MANUAL_PARKING_KEY_PRESS_DOWN', 3: 'EPB_MANUAL_PARKING_KEY_ERROR'}, 'is_signed_var': False, 'len': 2, 'name': 'epb_manual_parking_key', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
Epb_status_375_375::Epb_manual_parking_keyType Epbstatus375375::epb_manual_parking_key(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(5, 2);

  Epb_status_375_375::Epb_manual_parking_keyType ret =  static_cast<Epb_status_375_375::Epb_manual_parking_keyType>(x);
  return ret;
}

// config detail: {'bit': 2, 'enum': {0: 'EPB_FINAL_STATES_UNKNOW', 1: 'EPB_FINAL_STATES_PARKED', 2: 'EPB_FINAL_STATES_RELEASED', 3: 'EPB_FINAL_STATES_RESERVED'}, 'is_signed_var': False, 'len': 2, 'name': 'epb_final_states', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
Epb_status_375_375::Epb_final_statesType Epbstatus375375::epb_final_states(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(2, 2);

  Epb_status_375_375::Epb_final_statesType ret =  static_cast<Epb_status_375_375::Epb_final_statesType>(x);
  return ret;
}
}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
