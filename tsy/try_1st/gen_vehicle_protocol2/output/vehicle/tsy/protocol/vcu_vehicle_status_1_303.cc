/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/TSY/protocol/vcu_vehicle_status_1_303.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace TSY {

using ::apollo::drivers::canbus::Byte;

Vcuvehiclestatus1303::Vcuvehiclestatus1303() {}
const int32_t Vcuvehiclestatus1303::ID = 0x303;

void Vcuvehiclestatus1303::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_TSY()->mutable_vcu_vehicle_status_1_303()->set_epb_status_feedback(epb_status_feedback(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_vehicle_status_1_303()->set_vehicle_status_reserved(vehicle_status_reserved(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_vehicle_status_1_303()->set_vehicle_voltage(vehicle_voltage(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_vehicle_status_1_303()->set_bms_status(bms_status(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_vehicle_status_1_303()->set_vcu_ready_flag(vcu_ready_flag(bytes, length));
  chassis->mutable_TSY()->mutable_vcu_vehicle_status_1_303()->set_ignitionstatus(ignitionstatus(bytes, length));
}

// config detail: {'bit': 24, 'is_signed_var': False, 'len': 8, 'name': 'epb_status_feedback', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuvehiclestatus1303::epb_status_feedback(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 3);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 48, 'is_signed_var': False, 'len': 16, 'name': 'vehicle_status_reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuvehiclestatus1303::vehicle_status_reserved(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 6);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 32, 'is_signed_var': False, 'len': 16, 'name': 'vehicle_voltage', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'V', 'precision': 0.1, 'type': 'double'}
double Vcuvehiclestatus1303::vehicle_voltage(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 5);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 4);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.100000;
  return ret;
}

// config detail: {'bit': 16, 'is_signed_var': False, 'len': 8, 'name': 'bms_status', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuvehiclestatus1303::bms_status(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'vcu_ready_flag', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuvehiclestatus1303::vcu_ready_flag(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'bit': 8, 'is_signed_var': False, 'len': 8, 'name': 'ignitionstatus', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuvehiclestatus1303::ignitionstatus(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}
}  // namespace TSY
}  // namespace canbus
}  // namespace apollo
