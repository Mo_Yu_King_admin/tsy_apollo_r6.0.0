/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace tsy {

class Imueuler381 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;
  Imueuler381();
  void Parse(const std::uint8_t* bytes, int32_t length,
                     ChassisDetail* chassis) const override;

 private:

  // config detail: {'bit': 48, 'is_signed_var': True, 'len': 16, 'name': 'IMU_Euler_Reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-32768|32767]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  int imu_euler_reserved(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 32, 'is_signed_var': True, 'len': 16, 'name': 'IMU_Euler_Z', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-327.68|327.67]', 'physical_unit': 'deg', 'precision': 0.01, 'type': 'double'}
  double imu_euler_z(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 16, 'is_signed_var': True, 'len': 16, 'name': 'IMU_Euler_Y', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-327.68|327.67]', 'physical_unit': 'deg', 'precision': 0.01, 'type': 'double'}
  double imu_euler_y(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 0, 'is_signed_var': True, 'len': 16, 'name': 'IMU_Euler_X', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-327.68|327.67]', 'physical_unit': 'deg', 'precision': 0.01, 'type': 'double'}
  double imu_euler_x(const std::uint8_t* bytes, const int32_t length) const;
};

}  // namespace tsy
}  // namespace canbus
}  // namespace apollo


