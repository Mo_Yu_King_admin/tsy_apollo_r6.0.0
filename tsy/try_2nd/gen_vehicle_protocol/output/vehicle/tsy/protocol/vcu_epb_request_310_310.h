/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace tsy {

class Vcuepbrequest310310 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;
  Vcuepbrequest310310();
  void Parse(const std::uint8_t* bytes, int32_t length,
                     ChassisDetail* chassis) const override;

 private:

  // config detail: {'bit': 35, 'is_signed_var': False, 'len': 29, 'name': 'VCU_EPB_Request_Reserved2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  int vcu_epb_request_reserved2(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 3, 'is_signed_var': False, 'len': 32, 'name': 'VCU_EPB_Request_Reserved1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
  int vcu_epb_request_reserved1(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 2, 'enum': {0: 'FLAGVCU_EPB_REQUEST_VALID', 1: 'FLAGVCU_EPB_REQUEST_INVALID'}, 'is_signed_var': False, 'len': 1, 'name': 'FlagVCU_EPB_Request', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'enum'}
  Vcu_epb_request_310_310::Flagvcu_epb_requestType flagvcu_epb_request(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 0, 'enum': {0: 'VCU_EPB_PARKING_REQUEST_NOREQUEST', 1: 'VCU_EPB_PARKING_REQUEST_RELEASEBRAKEREQUEST', 2: 'VCU_EPB_PARKING_REQUEST_LOCKBRAKERQUEST', 3: 'VCU_EPB_PARKING_REQUEST_RESERVED'}, 'is_signed_var': False, 'len': 2, 'name': 'VCU_EPB_Parking_Request', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'enum'}
  Vcu_epb_request_310_310::Vcu_epb_parking_requestType vcu_epb_parking_request(const std::uint8_t* bytes, const int32_t length) const;
};

}  // namespace tsy
}  // namespace canbus
}  // namespace apollo


