/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/tsy/protocol/vcu_epb_request_310_310.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace tsy {

using ::apollo::drivers::canbus::Byte;

Vcuepbrequest310310::Vcuepbrequest310310() {}
const int32_t Vcuepbrequest310310::ID = 0x310;

void Vcuepbrequest310310::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_tsy()->mutable_vcu_epb_request_310_310()->set_vcu_epb_request_reserved2(vcu_epb_request_reserved2(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_epb_request_310_310()->set_vcu_epb_request_reserved1(vcu_epb_request_reserved1(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_epb_request_310_310()->set_flagvcu_epb_request(flagvcu_epb_request(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_epb_request_310_310()->set_vcu_epb_parking_request(vcu_epb_parking_request(bytes, length));
}

// config detail: {'bit': 35, 'is_signed_var': False, 'len': 29, 'name': 'vcu_epb_request_reserved2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuepbrequest310310::vcu_epb_request_reserved2(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 6);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 5);
  t = t2.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t3(bytes + 4);
  t = t3.get_byte(3, 5);
  x <<= 5;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 3, 'is_signed_var': False, 'len': 32, 'name': 'vcu_epb_request_reserved1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vcuepbrequest310310::vcu_epb_request_reserved1(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 4);
  int32_t x = t0.get_byte(0, 3);

  Byte t1(bytes + 3);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t3(bytes + 1);
  t = t3.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t4(bytes + 0);
  t = t4.get_byte(3, 5);
  x <<= 5;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 2, 'enum': {0: 'FLAGVCU_EPB_REQUEST_VALID', 1: 'FLAGVCU_EPB_REQUEST_INVALID'}, 'is_signed_var': False, 'len': 1, 'name': 'flagvcu_epb_request', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'enum'}
Vcu_epb_request_310_310::Flagvcu_epb_requestType Vcuepbrequest310310::flagvcu_epb_request(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(2, 1);

  Vcu_epb_request_310_310::Flagvcu_epb_requestType ret =  static_cast<Vcu_epb_request_310_310::Flagvcu_epb_requestType>(x);
  return ret;
}

// config detail: {'bit': 0, 'enum': {0: 'VCU_EPB_PARKING_REQUEST_NOREQUEST', 1: 'VCU_EPB_PARKING_REQUEST_RELEASEBRAKEREQUEST', 2: 'VCU_EPB_PARKING_REQUEST_LOCKBRAKERQUEST', 3: 'VCU_EPB_PARKING_REQUEST_RESERVED'}, 'is_signed_var': False, 'len': 2, 'name': 'vcu_epb_parking_request', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'enum'}
Vcu_epb_request_310_310::Vcu_epb_parking_requestType Vcuepbrequest310310::vcu_epb_parking_request(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 2);

  Vcu_epb_request_310_310::Vcu_epb_parking_requestType ret =  static_cast<Vcu_epb_request_310_310::Vcu_epb_parking_requestType>(x);
  return ret;
}
}  // namespace tsy
}  // namespace canbus
}  // namespace apollo
