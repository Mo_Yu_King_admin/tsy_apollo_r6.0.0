/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/tsy/protocol/vector__independent_sig_msg_c0000000.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace tsy {

using ::apollo::drivers::canbus::Byte;

Vectorindependentsigmsgc0000000::Vectorindependentsigmsgc0000000() {}
const int32_t Vectorindependentsigmsgc0000000::ID = 0x20000000;

void Vectorindependentsigmsgc0000000::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_imu_euler_yy_test(imu_euler_yy_test(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_imu_euler_y_test(imu_euler_y_test(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_imu_az_filter(imu_az_filter(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_imu_az_angle(imu_az_angle(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_ult_probe_info8(ult_probe_info8(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_ult_probe_info7(ult_probe_info7(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_ult_probe_info6(ult_probe_info6(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_ult_probe_info5(ult_probe_info5(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_ult_probe_info4(ult_probe_info4(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_ult_probe_info3(ult_probe_info3(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_ult_probe_info2(ult_probe_info2(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_ult_probe_info1(ult_probe_info1(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num12(vcu_probecmd_num12(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num11(vcu_probecmd_num11(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num10(vcu_probecmd_num10(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num9(vcu_probecmd_num9(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num8(vcu_probecmd_num8(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num7(vcu_probecmd_num7(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num6(vcu_probecmd_num6(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num5(vcu_probecmd_num5(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num4(vcu_probecmd_num4(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num3(vcu_probecmd_num3(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num2(vcu_probecmd_num2(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_probecmd_num1(vcu_probecmd_num1(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_machine_num(vcu_machine_num(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vcu_distance_cmd(vcu_distance_cmd(bytes, length));
  chassis->mutable_tsy()->mutable_vector__independent_sig_msg_c0000000()->set_vehicle_voltage(vehicle_voltage(bytes, length));
}

// config detail: {'bit': 0, 'is_signed_var': True, 'len': 16, 'name': 'imu_euler_yy_test', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-186.7776|186.7719]', 'physical_unit': 'deg', 'precision': 0.0057, 'type': 'double'}
double Vectorindependentsigmsgc0000000::imu_euler_yy_test(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.005700;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': True, 'len': 16, 'name': 'imu_euler_y_test', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-186.7776|186.7719]', 'physical_unit': 'deg', 'precision': 0.0057, 'type': 'double'}
double Vectorindependentsigmsgc0000000::imu_euler_y_test(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.005700;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'imu_az_filter', 'offset': -4.1746432, 'order': 'intel', 'physical_range': '[-1.8|1.8]', 'physical_unit': 'deg', 'precision': 0.0001274, 'type': 'double'}
double Vectorindependentsigmsgc0000000::imu_az_filter(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.000127 + -4.174643;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': True, 'len': 16, 'name': 'imu_az_angle', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-50|50]', 'physical_unit': 'deg', 'precision': 0.1, 'type': 'double'}
double Vectorindependentsigmsgc0000000::imu_az_angle(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.100000;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'ult_probe_info8', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|65535]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::ult_probe_info8(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'ult_probe_info7', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|65535]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::ult_probe_info7(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'ult_probe_info6', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|65535]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::ult_probe_info6(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'ult_probe_info5', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|65535]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::ult_probe_info5(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'ult_probe_info4', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|65535]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::ult_probe_info4(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'ult_probe_info3', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|65535]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::ult_probe_info3(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'ult_probe_info2', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|65535]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::ult_probe_info2(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'ult_probe_info1', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|65535]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::ult_probe_info1(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num12', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num12(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num11', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num11(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num10', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num10(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num9', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num9(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num8', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num8(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num7', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num7(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num6', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num6(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num5', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num5(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num4', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num4(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num3', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num3(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num2', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num2(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 1, 'name': 'vcu_probecmd_num1', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
bool Vectorindependentsigmsgc0000000::vcu_probecmd_num1(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  bool ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 4, 'name': 'vcu_machine_num', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|15]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::vcu_machine_num(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(5, 3);
  x <<= 3;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 8, 'name': 'vcu_distance_cmd', 'offset': 0.0, 'order': 'motorola', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Vectorindependentsigmsgc0000000::vcu_distance_cmd(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(1, 7);
  x <<= 7;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': False, 'len': 16, 'name': 'vehicle_voltage', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|100]', 'physical_unit': 'V', 'precision': 0.1, 'type': 'double'}
double Vectorindependentsigmsgc0000000::vehicle_voltage(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.100000;
  return ret;
}
}  // namespace tsy
}  // namespace canbus
}  // namespace apollo
