/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/tsy/protocol/imu_angular_281.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace tsy {

using ::apollo::drivers::canbus::Byte;

Imuangular281::Imuangular281() {}
const int32_t Imuangular281::ID = 0x281;

void Imuangular281::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_tsy()->mutable_imu_angular_281()->set_imu_gyro_reserved(imu_gyro_reserved(bytes, length));
  chassis->mutable_tsy()->mutable_imu_angular_281()->set_imu_gyro_z(imu_gyro_z(bytes, length));
  chassis->mutable_tsy()->mutable_imu_angular_281()->set_imu_gyro_y(imu_gyro_y(bytes, length));
  chassis->mutable_tsy()->mutable_imu_angular_281()->set_imu_gyro_x(imu_gyro_x(bytes, length));
}

// config detail: {'bit': 48, 'is_signed_var': True, 'len': 16, 'name': 'imu_gyro_reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Imuangular281::imu_gyro_reserved(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 6);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  int ret = x;
  return ret;
}

// config detail: {'bit': 32, 'is_signed_var': True, 'len': 16, 'name': 'imu_gyro_z', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-32.768|32.767]', 'physical_unit': 'rad/s', 'precision': 0.001, 'type': 'double'}
double Imuangular281::imu_gyro_z(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 5);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 4);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.001000;
  return ret;
}

// config detail: {'bit': 16, 'is_signed_var': True, 'len': 16, 'name': 'imu_gyro_y', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-32.768|32.767]', 'physical_unit': 'rad/s', 'precision': 0.001, 'type': 'double'}
double Imuangular281::imu_gyro_y(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 3);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 2);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.001000;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': True, 'len': 16, 'name': 'imu_gyro_x', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-32.768|32.767]', 'physical_unit': 'rad/s', 'precision': 0.001, 'type': 'double'}
double Imuangular281::imu_gyro_x(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.001000;
  return ret;
}
}  // namespace tsy
}  // namespace canbus
}  // namespace apollo
