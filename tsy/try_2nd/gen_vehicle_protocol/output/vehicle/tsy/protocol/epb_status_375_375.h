/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace tsy {

class Epbstatus375375 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;
  Epbstatus375375();
  void Parse(const std::uint8_t* bytes, int32_t length,
                     ChassisDetail* chassis) const override;

 private:

  // config detail: {'bit': 56, 'is_signed_var': False, 'len': 8, 'name': 'EPB_Status_Reserved4', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
  int epb_status_reserved4(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 7, 'is_signed_var': False, 'len': 1, 'name': 'EPB_Status_Reserved2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'bool'}
  bool epb_status_reserved2(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 24, 'is_signed_var': False, 'len': 32, 'name': 'EPB_Status_Reserved3', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
  int epb_status_reserved3(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 0, 'is_signed_var': False, 'len': 2, 'name': 'EPB_Status_Reserved1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
  int epb_status_reserved1(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 8, 'is_signed_var': False, 'len': 16, 'name': 'EPB_IMU_Ax_Feedback', 'offset': -1.7, 'order': 'intel', 'physical_range': '[-1.7|1.7]', 'physical_unit': 'g', 'precision': 0.0034, 'type': 'double'}
  double epb_imu_ax_feedback(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 4, 'enum': {0: 'EPB_FAULT_NO_FAULT', 1: 'EPB_FAULT_FAULT'}, 'is_signed_var': False, 'len': 1, 'name': 'EPB_FAULT', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
  Epb_status_375_375::Epb_faultType epb_fault(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 5, 'enum': {0: 'EPB_MANUAL_PARKING_KEY_RELEASE', 1: 'EPB_MANUAL_PARKING_KEY_PULL_UP', 2: 'EPB_MANUAL_PARKING_KEY_PRESS_DOWN', 3: 'EPB_MANUAL_PARKING_KEY_ERROR'}, 'is_signed_var': False, 'len': 2, 'name': 'EPB_MANUAL_PARKING_KEY', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
  Epb_status_375_375::Epb_manual_parking_keyType epb_manual_parking_key(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 2, 'enum': {0: 'EPB_FINAL_STATES_UNKNOW', 1: 'EPB_FINAL_STATES_PARKED', 2: 'EPB_FINAL_STATES_RELEASED', 3: 'EPB_FINAL_STATES_RESERVED'}, 'is_signed_var': False, 'len': 2, 'name': 'EPB_FINAL_STATES', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
  Epb_status_375_375::Epb_final_statesType epb_final_states(const std::uint8_t* bytes, const int32_t length) const;
};

}  // namespace tsy
}  // namespace canbus
}  // namespace apollo


