/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/tsy/protocol/imu_euler_381.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace tsy {

using ::apollo::drivers::canbus::Byte;

Imueuler381::Imueuler381() {}
const int32_t Imueuler381::ID = 0x381;

void Imueuler381::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_tsy()->mutable_imu_euler_381()->set_imu_euler_reserved(imu_euler_reserved(bytes, length));
  chassis->mutable_tsy()->mutable_imu_euler_381()->set_imu_euler_z(imu_euler_z(bytes, length));
  chassis->mutable_tsy()->mutable_imu_euler_381()->set_imu_euler_y(imu_euler_y(bytes, length));
  chassis->mutable_tsy()->mutable_imu_euler_381()->set_imu_euler_x(imu_euler_x(bytes, length));
}

// config detail: {'bit': 48, 'is_signed_var': True, 'len': 16, 'name': 'imu_euler_reserved', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-32768|32767]', 'physical_unit': 'N/A', 'precision': 1.0, 'type': 'int'}
int Imueuler381::imu_euler_reserved(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 6);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  int ret = x;
  return ret;
}

// config detail: {'bit': 32, 'is_signed_var': True, 'len': 16, 'name': 'imu_euler_z', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-186.7776|186.7719]', 'physical_unit': 'deg', 'precision': 0.0057, 'type': 'double'}
double Imueuler381::imu_euler_z(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 5);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 4);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.005700;
  return ret;
}

// config detail: {'bit': 16, 'is_signed_var': True, 'len': 16, 'name': 'imu_euler_y', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-186.7776|186.7719]', 'physical_unit': 'deg', 'precision': 0.0057, 'type': 'double'}
double Imueuler381::imu_euler_y(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 3);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 2);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.005700;
  return ret;
}

// config detail: {'bit': 0, 'is_signed_var': True, 'len': 16, 'name': 'imu_euler_x', 'offset': 0.0, 'order': 'intel', 'physical_range': '[-186.7776|186.7719]', 'physical_unit': 'deg', 'precision': 0.0057, 'type': 'double'}
double Imueuler381::imu_euler_x(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 0);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  x <<= 16;
  x >>= 16;

  double ret = x * 0.005700;
  return ret;
}
}  // namespace tsy
}  // namespace canbus
}  // namespace apollo
