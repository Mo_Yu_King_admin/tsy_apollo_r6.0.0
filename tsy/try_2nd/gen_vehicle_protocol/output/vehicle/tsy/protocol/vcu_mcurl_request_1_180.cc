/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/tsy/protocol/vcu_mcurl_request_1_180.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace tsy {

using ::apollo::drivers::canbus::Byte;

Vcumcurlrequest1180::Vcumcurlrequest1180() {}
const int32_t Vcumcurlrequest1180::ID = 0x180;

void Vcumcurlrequest1180::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_tsy()->mutable_vcu_mcurl_request_1_180()->set_rl_vcu_motor_request_valid(rl_vcu_motor_request_valid(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurl_request_1_180()->set_rl_torque_req(rl_torque_req(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurl_request_1_180()->set_rl_speed_req(rl_speed_req(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurl_request_1_180()->set_rl_reserved_2(rl_reserved_2(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurl_request_1_180()->set_rl_reserved_1(rl_reserved_1(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurl_request_1_180()->set_rl_drivemode(rl_drivemode(bytes, length));
}

// config detail: {'bit': 0, 'enum': {0: 'RL_VCU_MOTOR_REQUEST_VALID_INVALID', 1: 'RL_VCU_MOTOR_REQUEST_VALID_VALID'}, 'is_signed_var': False, 'len': 1, 'name': 'rl_vcu_motor_request_valid', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
Vcu_mcurl_request_1_180::Rl_vcu_motor_request_validType Vcumcurlrequest1180::rl_vcu_motor_request_valid(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Vcu_mcurl_request_1_180::Rl_vcu_motor_request_validType ret =  static_cast<Vcu_mcurl_request_1_180::Rl_vcu_motor_request_validType>(x);
  return ret;
}

// config detail: {'bit': 8, 'is_signed_var': False, 'len': 16, 'name': 'rl_torque_req', 'offset': -1000.0, 'order': 'intel', 'physical_range': '[-1000|1000]', 'physical_unit': '0.1Nm', 'precision': 0.1, 'type': 'double'}
double Vcumcurlrequest1180::rl_torque_req(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.100000 + -1000.000000;
  return ret;
}

// config detail: {'bit': 24, 'is_signed_var': False, 'len': 16, 'name': 'rl_speed_req', 'offset': -7000.0, 'order': 'intel', 'physical_range': '[-7000|7000]', 'physical_unit': '1RPM', 'precision': 1.0, 'type': 'int'}
int Vcumcurlrequest1180::rl_speed_req(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 4);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 3);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x + -7000.000000;
  return ret;
}

// config detail: {'bit': 40, 'is_signed_var': False, 'len': 24, 'name': 'rl_reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vcumcurlrequest1180::rl_reserved_2(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 6);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 5);
  t = t2.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 3, 'is_signed_var': False, 'len': 5, 'name': 'rl_reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|31]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vcumcurlrequest1180::rl_reserved_1(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(3, 5);

  int ret = x;
  return ret;
}

// config detail: {'bit': 1, 'is_signed_var': False, 'len': 2, 'name': 'rl_drivemode', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vcumcurlrequest1180::rl_drivemode(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(1, 2);

  int ret = x;
  return ret;
}
}  // namespace tsy
}  // namespace canbus
}  // namespace apollo
