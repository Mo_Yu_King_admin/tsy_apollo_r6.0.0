/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/tsy/protocol/vcu_mcurr_request_1_190.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace tsy {

using ::apollo::drivers::canbus::Byte;

Vcumcurrrequest1190::Vcumcurrrequest1190() {}
const int32_t Vcumcurrrequest1190::ID = 0x190;

void Vcumcurrrequest1190::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_tsy()->mutable_vcu_mcurr_request_1_190()->set_rr_vcu_motor_request_valid(rr_vcu_motor_request_valid(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurr_request_1_190()->set_rr_torque_req(rr_torque_req(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurr_request_1_190()->set_rr_speed_req(rr_speed_req(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurr_request_1_190()->set_rr_reserved_2(rr_reserved_2(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurr_request_1_190()->set_rr_reserved_1(rr_reserved_1(bytes, length));
  chassis->mutable_tsy()->mutable_vcu_mcurr_request_1_190()->set_rr_drivemode(rr_drivemode(bytes, length));
}

// config detail: {'bit': 0, 'enum': {0: 'RR_VCU_MOTOR_REQUEST_VALID_INVALID', 1: 'RR_VCU_MOTOR_REQUEST_VALID_VALID'}, 'is_signed_var': False, 'len': 1, 'name': 'rr_vcu_motor_request_valid', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
Vcu_mcurr_request_1_190::Rr_vcu_motor_request_validType Vcumcurrrequest1190::rr_vcu_motor_request_valid(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 1);

  Vcu_mcurr_request_1_190::Rr_vcu_motor_request_validType ret =  static_cast<Vcu_mcurr_request_1_190::Rr_vcu_motor_request_validType>(x);
  return ret;
}

// config detail: {'bit': 8, 'is_signed_var': False, 'len': 16, 'name': 'rr_torque_req', 'offset': -1000.0, 'order': 'intel', 'physical_range': '[-1000|1000]', 'physical_unit': '0.1Nm', 'precision': 0.1, 'type': 'double'}
double Vcumcurrrequest1190::rr_torque_req(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.100000 + -1000.000000;
  return ret;
}

// config detail: {'bit': 24, 'is_signed_var': False, 'len': 16, 'name': 'rr_speed_req', 'offset': -7000.0, 'order': 'intel', 'physical_range': '[-7000|7000]', 'physical_unit': '1RPM', 'precision': 1.0, 'type': 'int'}
int Vcumcurrrequest1190::rr_speed_req(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 4);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 3);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x + -7000.000000;
  return ret;
}

// config detail: {'bit': 40, 'is_signed_var': False, 'len': 24, 'name': 'rr_reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vcumcurrrequest1190::rr_reserved_2(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 6);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 5);
  t = t2.get_byte(0, 8);
  x <<= 8;
  x |= t;

  int ret = x;
  return ret;
}

// config detail: {'bit': 3, 'is_signed_var': False, 'len': 5, 'name': 'rr_reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|31]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vcumcurrrequest1190::rr_reserved_1(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(3, 5);

  int ret = x;
  return ret;
}

// config detail: {'bit': 1, 'is_signed_var': False, 'len': 2, 'name': 'rr_drivemode', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
int Vcumcurrrequest1190::rr_drivemode(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(1, 2);

  int ret = x;
  return ret;
}
}  // namespace tsy
}  // namespace canbus
}  // namespace apollo
