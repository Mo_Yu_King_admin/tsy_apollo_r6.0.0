/******************************************************************************
 * Copyright 2019 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace tsy {

class Vcumcurrrequest1190 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;
  Vcumcurrrequest1190();
  void Parse(const std::uint8_t* bytes, int32_t length,
                     ChassisDetail* chassis) const override;

 private:

  // config detail: {'bit': 0, 'enum': {0: 'RR_VCU_MOTOR_REQUEST_VALID_INVALID', 1: 'RR_VCU_MOTOR_REQUEST_VALID_VALID'}, 'is_signed_var': False, 'len': 1, 'name': 'RR_VCU_Motor_Request_Valid', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|1]', 'physical_unit': '', 'precision': 1.0, 'type': 'enum'}
  Vcu_mcurr_request_1_190::Rr_vcu_motor_request_validType rr_vcu_motor_request_valid(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 8, 'is_signed_var': False, 'len': 16, 'name': 'RR_Torque_Req', 'offset': -1000.0, 'order': 'intel', 'physical_range': '[-1000|1000]', 'physical_unit': '0.1Nm', 'precision': 0.1, 'type': 'double'}
  double rr_torque_req(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 24, 'is_signed_var': False, 'len': 16, 'name': 'RR_Speed_Req', 'offset': -7000.0, 'order': 'intel', 'physical_range': '[-7000|7000]', 'physical_unit': '1RPM', 'precision': 1.0, 'type': 'int'}
  int rr_speed_req(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 40, 'is_signed_var': False, 'len': 24, 'name': 'RR_Reserved_2', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|255]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
  int rr_reserved_2(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 3, 'is_signed_var': False, 'len': 5, 'name': 'RR_Reserved_1', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|31]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
  int rr_reserved_1(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'bit': 1, 'is_signed_var': False, 'len': 2, 'name': 'RR_DriveMode', 'offset': 0.0, 'order': 'intel', 'physical_range': '[0|3]', 'physical_unit': '', 'precision': 1.0, 'type': 'int'}
  int rr_drivemode(const std::uint8_t* bytes, const int32_t length) const;
};

}  // namespace tsy
}  // namespace canbus
}  // namespace apollo


