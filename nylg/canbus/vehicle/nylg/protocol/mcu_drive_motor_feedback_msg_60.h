/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef MODULES_CANBUS_VEHICLE_NYLG_PROTOCOL_MCU_DRIVE_MOTOR_FEEDBACK_MSG_60_H_
#define MODULES_CANBUS_VEHICLE_NYLG_PROTOCOL_MCU_DRIVE_MOTOR_FEEDBACK_MSG_60_H_

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace nylg {

class Mcudrivemotorfeedbackmsg60 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;
  Mcudrivemotorfeedbackmsg60();
  void Parse(const std::uint8_t* bytes, int32_t length,
                     ChassisDetail* chassis) const override;

 private:

  // config detail: {'name': 'Clamping_brake_status', 'offset': 0.0, 'precision': 1.0, 'len': 1, 'is_signed_var': False, 'physical_range': '[0|1]', 'bit': 43, 'type': 'bool', 'order': 'intel', 'physical_unit': 'N/A'}
  bool clamping_brake_status(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_Reserved_1', 'offset': 0.0, 'precision': 1.0, 'len': 20, 'is_signed_var': False, 'physical_range': '[0|1048575]', 'bit': 44, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  int mcu_reserved_1(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_Motor_Error_Grade', 'offset': 0.0, 'precision': 1.0, 'len': 2, 'is_signed_var': False, 'physical_range': '[0|3]', 'bit': 41, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  int mcu_motor_error_grade(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_Energy_Recovery_State', 'offset': 0.0, 'precision': 1.0, 'len': 1, 'is_signed_var': False, 'physical_range': '[0|1]', 'bit': 40, 'type': 'bool', 'order': 'intel', 'physical_unit': 'N/A'}
  bool mcu_energy_recovery_state(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_ControlTemp', 'offset': -50.0, 'precision': 1.0, 'len': 8, 'is_signed_var': False, 'physical_range': '[-50|205]', 'bit': 32, 'type': 'int', 'order': 'intel', 'physical_unit': '\xb6\xc8'}
  int mcu_controltemp(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_Idc', 'offset': -1000.0, 'precision': 0.1, 'len': 16, 'is_signed_var': False, 'physical_range': '[-1000|5553.5]', 'bit': 16, 'type': 'double', 'order': 'intel', 'physical_unit': 'A'}
  double mcu_idc(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_Udc', 'offset': 0.0, 'precision': 0.1, 'len': 16, 'is_signed_var': False, 'physical_range': '[0|6553.5]', 'bit': 0, 'type': 'double', 'order': 'intel', 'physical_unit': 'V'}
  double mcu_udc(const std::uint8_t* bytes, const int32_t length) const;
};

}  // namespace nylg
}  // namespace canbus
}  // namespace apollo

#endif  // MODULES_CANBUS_VEHICL_NYLG_PROTOCOL_MCU_DRIVE_MOTOR_FEEDBACK_MSG_60_H_
