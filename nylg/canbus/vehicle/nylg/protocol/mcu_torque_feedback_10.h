/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef MODULES_CANBUS_VEHICLE_NYLG_PROTOCOL_MCU_TORQUE_FEEDBACK_10_H_
#define MODULES_CANBUS_VEHICLE_NYLG_PROTOCOL_MCU_TORQUE_FEEDBACK_10_H_

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace nylg {

class Mcutorquefeedback10 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;
  Mcutorquefeedback10();
  void Parse(const std::uint8_t* bytes, int32_t length,
                     ChassisDetail* chassis) const override;

 private:

  // config detail: {'name': 'MCU_ERRORCODE', 'offset': 0.0, 'precision': 1.0, 'len': 8, 'is_signed_var': False, 'physical_range': '[0|255]', 'bit': 56, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  int mcu_errorcode(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_MOTORTEMP', 'offset': -50.0, 'precision': 1.0, 'len': 8, 'is_signed_var': False, 'physical_range': '[-50|205]', 'bit': 48, 'type': 'int', 'order': 'intel', 'physical_unit': '\xb6\xc8'}
  int mcu_motortemp(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_CURRENT', 'offset': -500.0, 'precision': 1.0, 'len': 12, 'is_signed_var': False, 'physical_range': '[-500|500]', 'bit': 36, 'type': 'int', 'order': 'intel', 'physical_unit': 'A'}
  int mcu_current(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_TORQUE', 'offset': -1000.0, 'precision': 0.1, 'len': 16, 'is_signed_var': False, 'physical_range': '[-1000|1000]', 'bit': 20, 'type': 'double', 'order': 'intel', 'physical_unit': 'N.m'}
  double mcu_torque(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_SPEED', 'offset': -10000.0, 'precision': 0.1, 'len': 18, 'is_signed_var': False, 'physical_range': '[-10000|10000]', 'bit': 2, 'type': 'double', 'order': 'intel', 'physical_unit': 'rpm'}
  double mcu_speed(const std::uint8_t* bytes, const int32_t length) const;

  // config detail: {'name': 'MCU_Shift', 'offset': 0.0, 'precision': 1.0, 'len': 2, 'is_signed_var': False, 'physical_range': '[0|3]', 'bit': 0, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  int mcu_shift(const std::uint8_t* bytes, const int32_t length) const;
};

}  // namespace nylg
}  // namespace canbus
}  // namespace apollo

#endif  // MODULES_CANBUS_VEHICL_NYLG_PROTOCOL_MCU_TORQUE_FEEDBACK_10_H_
