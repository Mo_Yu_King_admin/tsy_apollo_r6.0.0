/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/nylg/protocol/mcu_torque_feedback_10.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace nylg {

using ::apollo::drivers::canbus::Byte;

Mcutorquefeedback10::Mcutorquefeedback10() {}
const int32_t Mcutorquefeedback10::ID = 0x10;

void Mcutorquefeedback10::Parse(const std::uint8_t* bytes, int32_t length,
                         ChassisDetail* chassis) const {
  chassis->mutable_nylg()->mutable_mcu_torque_feedback_10()->set_mcu_errorcode(mcu_errorcode(bytes, length));
  chassis->mutable_nylg()->mutable_mcu_torque_feedback_10()->set_mcu_motortemp(mcu_motortemp(bytes, length));
  chassis->mutable_nylg()->mutable_mcu_torque_feedback_10()->set_mcu_current(mcu_current(bytes, length));
  chassis->mutable_nylg()->mutable_mcu_torque_feedback_10()->set_mcu_torque(mcu_torque(bytes, length));
  chassis->mutable_nylg()->mutable_mcu_torque_feedback_10()->set_mcu_speed(mcu_speed(bytes, length));
  chassis->mutable_nylg()->mutable_mcu_torque_feedback_10()->set_mcu_shift(mcu_shift(bytes, length));
}

// config detail: {'name': 'mcu_errorcode', 'offset': 0.0, 'precision': 1.0, 'len': 8, 'is_signed_var': False, 'physical_range': '[0|255]', 'bit': 56, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
int Mcutorquefeedback10::mcu_errorcode(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 7);
  int32_t x = t0.get_byte(0, 8);

  int ret = x;
  return ret;
}

// config detail: {'name': 'mcu_motortemp', 'offset': -50.0, 'precision': 1.0, 'len': 8, 'is_signed_var': False, 'physical_range': '[-50|205]', 'bit': 48, 'type': 'int', 'order': 'intel', 'physical_unit': '\xb6\xc8'}
int Mcutorquefeedback10::mcu_motortemp(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 6);
  int32_t x = t0.get_byte(0, 8);

  int ret = x + -50.000000;
  return ret;
}

// config detail: {'name': 'mcu_current', 'offset': -500.0, 'precision': 1.0, 'len': 12, 'is_signed_var': False, 'physical_range': '[-500|500]', 'bit': 36, 'type': 'int', 'order': 'intel', 'physical_unit': 'A'}
int Mcutorquefeedback10::mcu_current(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 5);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 4);
  int32_t t = t1.get_byte(4, 4);
  x <<= 4;
  x |= t;

  int ret = x + -500.000000;
  return ret;
}

// config detail: {'name': 'mcu_torque', 'offset': -1000.0, 'precision': 0.1, 'len': 16, 'is_signed_var': False, 'physical_range': '[-1000|1000]', 'bit': 20, 'type': 'double', 'order': 'intel', 'physical_unit': 'N.m'}
double Mcutorquefeedback10::mcu_torque(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 4);
  int32_t x = t0.get_byte(0, 4);

  Byte t1(bytes + 3);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 2);
  t = t2.get_byte(4, 4);
  x <<= 4;
  x |= t;

  double ret = x * 0.100000 + -1000.000000;
  return ret;
}

// config detail: {'name': 'mcu_speed', 'offset': -10000.0, 'precision': 0.1, 'len': 18, 'is_signed_var': False, 'physical_range': '[-10000|10000]', 'bit': 2, 'type': 'double', 'order': 'intel', 'physical_unit': 'rpm'}
double Mcutorquefeedback10::mcu_speed(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 4);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  Byte t2(bytes + 0);
  t = t2.get_byte(2, 6);
  x <<= 6;
  x |= t;

  double ret = x * 0.100000 + -10000.000000;
  return ret;
}

// config detail: {'name': 'mcu_shift', 'offset': 0.0, 'precision': 1.0, 'len': 2, 'is_signed_var': False, 'physical_range': '[0|3]', 'bit': 0, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
int Mcutorquefeedback10::mcu_shift(const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 2);

  int ret = x;
  return ret;
}
}  // namespace nylg
}  // namespace canbus
}  // namespace apollo
