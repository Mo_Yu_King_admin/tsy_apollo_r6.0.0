/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef MODULES_CANBUS_VEHICLE_NYLG_PROTOCOL_VCU_MCU_REQUEST_160_H_
#define MODULES_CANBUS_VEHICLE_NYLG_PROTOCOL_VCU_MCU_REQUEST_160_H_

#include "modules/drivers/canbus/can_comm/protocol_data.h"
#include "modules/canbus/proto/chassis_detail.pb.h"

namespace apollo {
namespace canbus {
namespace nylg {

class Vcumcurequest160 : public ::apollo::drivers::canbus::ProtocolData<
                    ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;

  Vcumcurequest160();

  uint32_t GetPeriod() const override;

  void UpdateData(uint8_t* data) override;

  void Reset() override;

  // config detail: {'name': 'Clamping_brake_req', 'offset': 0.0, 'precision': 1.0, 'len': 1, 'is_signed_var': False, 'physical_range': '[0|1]', 'bit': 3, 'type': 'bool', 'order': 'intel', 'physical_unit': 'N/A'}
  Vcumcurequest160* set_clamping_brake_req(bool clamping_brake_req);

  // config detail: {'name': 'MCU_Reserved_2', 'offset': 0.0, 'precision': 1.0, 'len': 24, 'is_signed_var': False, 'physical_range': '[0|255]', 'bit': 40, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  Vcumcurequest160* set_mcu_reserved_2(int mcu_reserved_2);

  // config detail: {'name': 'MCU_Speed_Req', 'offset': -7000.0, 'precision': 1.0, 'len': 16, 'is_signed_var': False, 'physical_range': '[-7000|7000]', 'bit': 24, 'type': 'int', 'order': 'intel', 'physical_unit': 'RPM'}
  Vcumcurequest160* set_mcu_speed_req(int mcu_speed_req);

  // config detail: {'name': 'MCU_Torque_Req', 'offset': -1000.0, 'precision': 0.1, 'len': 16, 'is_signed_var': False, 'physical_range': '[-1000|1000]', 'bit': 8, 'type': 'double', 'order': 'intel', 'physical_unit': 'N.m'}
  Vcumcurequest160* set_mcu_torque_req(double mcu_torque_req);

  // config detail: {'name': 'MCU_Reserved_1', 'offset': 0.0, 'precision': 1.0, 'len': 4, 'is_signed_var': False, 'physical_range': '[0|15]', 'bit': 4, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  Vcumcurequest160* set_mcu_reserved_1(int mcu_reserved_1);

  // config detail: {'name': 'MCU_DriveMode', 'offset': 0.0, 'precision': 1.0, 'len': 2, 'is_signed_var': False, 'physical_range': '[0|3]', 'bit': 1, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  Vcumcurequest160* set_mcu_drivemode(int mcu_drivemode);

  // config detail: {'name': 'MCU_VCU_Motor_Request_Valid', 'offset': 0.0, 'precision': 1.0, 'len': 1, 'is_signed_var': False, 'physical_range': '[0|1]', 'bit': 0, 'type': 'bool', 'order': 'intel', 'physical_unit': 'N/A'}
  Vcumcurequest160* set_mcu_vcu_motor_request_valid(bool mcu_vcu_motor_request_valid);

 private:

  // config detail: {'name': 'Clamping_brake_req', 'offset': 0.0, 'precision': 1.0, 'len': 1, 'is_signed_var': False, 'physical_range': '[0|1]', 'bit': 3, 'type': 'bool', 'order': 'intel', 'physical_unit': 'N/A'}
  void set_p_clamping_brake_req(uint8_t* data, bool clamping_brake_req);

  // config detail: {'name': 'MCU_Reserved_2', 'offset': 0.0, 'precision': 1.0, 'len': 24, 'is_signed_var': False, 'physical_range': '[0|255]', 'bit': 40, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  void set_p_mcu_reserved_2(uint8_t* data, int mcu_reserved_2);

  // config detail: {'name': 'MCU_Speed_Req', 'offset': -7000.0, 'precision': 1.0, 'len': 16, 'is_signed_var': False, 'physical_range': '[-7000|7000]', 'bit': 24, 'type': 'int', 'order': 'intel', 'physical_unit': 'RPM'}
  void set_p_mcu_speed_req(uint8_t* data, int mcu_speed_req);

  // config detail: {'name': 'MCU_Torque_Req', 'offset': -1000.0, 'precision': 0.1, 'len': 16, 'is_signed_var': False, 'physical_range': '[-1000|1000]', 'bit': 8, 'type': 'double', 'order': 'intel', 'physical_unit': 'N.m'}
  void set_p_mcu_torque_req(uint8_t* data, double mcu_torque_req);

  // config detail: {'name': 'MCU_Reserved_1', 'offset': 0.0, 'precision': 1.0, 'len': 4, 'is_signed_var': False, 'physical_range': '[0|15]', 'bit': 4, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  void set_p_mcu_reserved_1(uint8_t* data, int mcu_reserved_1);

  // config detail: {'name': 'MCU_DriveMode', 'offset': 0.0, 'precision': 1.0, 'len': 2, 'is_signed_var': False, 'physical_range': '[0|3]', 'bit': 1, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
  void set_p_mcu_drivemode(uint8_t* data, int mcu_drivemode);

  // config detail: {'name': 'MCU_VCU_Motor_Request_Valid', 'offset': 0.0, 'precision': 1.0, 'len': 1, 'is_signed_var': False, 'physical_range': '[0|1]', 'bit': 0, 'type': 'bool', 'order': 'intel', 'physical_unit': 'N/A'}
  void set_p_mcu_vcu_motor_request_valid(uint8_t* data, bool mcu_vcu_motor_request_valid);

 private:
  bool clamping_brake_req_;
  int mcu_reserved_2_;
  int mcu_speed_req_;
  double mcu_torque_req_;
  int mcu_reserved_1_;
  int mcu_drivemode_;
  bool mcu_vcu_motor_request_valid_;
};

}  // namespace nylg
}  // namespace canbus
}  // namespace apollo

#endif  // MODULES_CANBUS_VEHICL_NYLG_PROTOCOL_VCU_MCU_REQUEST_160_H_
