/******************************************************************************
 * Copyright 2017 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/nylg/protocol/vcu_mcu_request_160.h"

#include "modules/drivers/canbus/common/byte.h"

namespace apollo {
namespace canbus {
namespace nylg {

using ::apollo::drivers::canbus::Byte;

const int32_t Vcumcurequest160::ID = 0x160;

// public
Vcumcurequest160::Vcumcurequest160() { Reset(); }

uint32_t Vcumcurequest160::GetPeriod() const {
  // TODO modify every protocol's period manually
  static const uint32_t PERIOD = 20 * 1000;
  return PERIOD;
}

void Vcumcurequest160::UpdateData(uint8_t* data) {
  set_p_clamping_brake_req(data, clamping_brake_req_);
  set_p_mcu_reserved_2(data, mcu_reserved_2_);
  set_p_mcu_speed_req(data, mcu_speed_req_);
  set_p_mcu_torque_req(data, mcu_torque_req_);
  set_p_mcu_reserved_1(data, mcu_reserved_1_);
  set_p_mcu_drivemode(data, mcu_drivemode_);
  set_p_mcu_vcu_motor_request_valid(data, mcu_vcu_motor_request_valid_);
}

void Vcumcurequest160::Reset() {
  // TODO you should check this manually
  clamping_brake_req_ = false;
  mcu_reserved_2_ = 0;
  mcu_speed_req_ = 0;
  mcu_torque_req_ = 0.0;
  mcu_reserved_1_ = 0;
  mcu_drivemode_ = 0;
  mcu_vcu_motor_request_valid_ = false;
}

Vcumcurequest160* Vcumcurequest160::set_clamping_brake_req(
    bool clamping_brake_req) {
  clamping_brake_req_ = clamping_brake_req;
  return this;
 }

// config detail: {'name': 'Clamping_brake_req', 'offset': 0.0, 'precision': 1.0, 'len': 1, 'is_signed_var': False, 'physical_range': '[0|1]', 'bit': 3, 'type': 'bool', 'order': 'intel', 'physical_unit': 'N/A'}
void Vcumcurequest160::set_p_clamping_brake_req(uint8_t* data,
    bool clamping_brake_req) {
  int x = clamping_brake_req;

  Byte to_set(data + 0);
  to_set.set_value(x, 3, 1);
}


Vcumcurequest160* Vcumcurequest160::set_mcu_reserved_2(
    int mcu_reserved_2) {
  mcu_reserved_2_ = mcu_reserved_2;
  return this;
 }

// config detail: {'name': 'MCU_Reserved_2', 'offset': 0.0, 'precision': 1.0, 'len': 24, 'is_signed_var': False, 'physical_range': '[0|255]', 'bit': 40, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
void Vcumcurequest160::set_p_mcu_reserved_2(uint8_t* data,
    int mcu_reserved_2) {
  mcu_reserved_2 = ProtocolData::BoundedValue(0, 255, mcu_reserved_2);
  int x = mcu_reserved_2;
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 5);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 6);
  to_set1.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set2(data + 7);
  to_set2.set_value(t, 0, 8);
}


Vcumcurequest160* Vcumcurequest160::set_mcu_speed_req(
    int mcu_speed_req) {
  mcu_speed_req_ = mcu_speed_req;
  return this;
 }

// config detail: {'name': 'MCU_Speed_Req', 'offset': -7000.0, 'precision': 1.0, 'len': 16, 'is_signed_var': False, 'physical_range': '[-7000|7000]', 'bit': 24, 'type': 'int', 'order': 'intel', 'physical_unit': 'RPM'}
void Vcumcurequest160::set_p_mcu_speed_req(uint8_t* data,
    int mcu_speed_req) {
  mcu_speed_req = ProtocolData::BoundedValue(-7000, 7000, mcu_speed_req);
  int x = (mcu_speed_req - -7000.000000);
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 3);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 4);
  to_set1.set_value(t, 0, 8);
}


Vcumcurequest160* Vcumcurequest160::set_mcu_torque_req(
    double mcu_torque_req) {
  mcu_torque_req_ = mcu_torque_req;
  return this;
 }

// config detail: {'name': 'MCU_Torque_Req', 'offset': -1000.0, 'precision': 0.1, 'len': 16, 'is_signed_var': False, 'physical_range': '[-1000|1000]', 'bit': 8, 'type': 'double', 'order': 'intel', 'physical_unit': 'N.m'}
void Vcumcurequest160::set_p_mcu_torque_req(uint8_t* data,
    double mcu_torque_req) {
  mcu_torque_req = ProtocolData::BoundedValue(-1000.0, 1000.0, mcu_torque_req);
  int x = (mcu_torque_req - -1000.000000) / 0.100000;
  uint8_t t = 0;

  t = x & 0xFF;
  Byte to_set0(data + 1);
  to_set0.set_value(t, 0, 8);
  x >>= 8;

  t = x & 0xFF;
  Byte to_set1(data + 2);
  to_set1.set_value(t, 0, 8);
}


Vcumcurequest160* Vcumcurequest160::set_mcu_reserved_1(
    int mcu_reserved_1) {
  mcu_reserved_1_ = mcu_reserved_1;
  return this;
 }

// config detail: {'name': 'MCU_Reserved_1', 'offset': 0.0, 'precision': 1.0, 'len': 4, 'is_signed_var': False, 'physical_range': '[0|15]', 'bit': 4, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
void Vcumcurequest160::set_p_mcu_reserved_1(uint8_t* data,
    int mcu_reserved_1) {
  mcu_reserved_1 = ProtocolData::BoundedValue(0, 15, mcu_reserved_1);
  int x = mcu_reserved_1;

  Byte to_set(data + 0);
  to_set.set_value(x, 4, 4);
}


Vcumcurequest160* Vcumcurequest160::set_mcu_drivemode(
    int mcu_drivemode) {
  mcu_drivemode_ = mcu_drivemode;
  return this;
 }

// config detail: {'name': 'MCU_DriveMode', 'offset': 0.0, 'precision': 1.0, 'len': 2, 'is_signed_var': False, 'physical_range': '[0|3]', 'bit': 1, 'type': 'int', 'order': 'intel', 'physical_unit': 'N/A'}
void Vcumcurequest160::set_p_mcu_drivemode(uint8_t* data,
    int mcu_drivemode) {
  mcu_drivemode = ProtocolData::BoundedValue(0, 3, mcu_drivemode);
  int x = mcu_drivemode;

  Byte to_set(data + 0);
  to_set.set_value(x, 1, 2);
}


Vcumcurequest160* Vcumcurequest160::set_mcu_vcu_motor_request_valid(
    bool mcu_vcu_motor_request_valid) {
  mcu_vcu_motor_request_valid_ = mcu_vcu_motor_request_valid;
  return this;
 }

// config detail: {'name': 'MCU_VCU_Motor_Request_Valid', 'offset': 0.0, 'precision': 1.0, 'len': 1, 'is_signed_var': False, 'physical_range': '[0|1]', 'bit': 0, 'type': 'bool', 'order': 'intel', 'physical_unit': 'N/A'}
void Vcumcurequest160::set_p_mcu_vcu_motor_request_valid(uint8_t* data,
    bool mcu_vcu_motor_request_valid) {
  int x = mcu_vcu_motor_request_valid;

  Byte to_set(data + 0);
  to_set.set_value(x, 0, 1);
}

}  // namespace nylg
}  // namespace canbus
}  // namespace apollo
