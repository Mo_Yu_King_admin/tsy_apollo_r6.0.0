/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agribot/protocol/accel_rpt_2e3.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace agribot {

using ::apollo::drivers::canbus::Byte;

Accelrpt2e3::Accelrpt2e3() {}
const int32_t Accelrpt2e3::ID = 0x2E3;

void Accelrpt2e3::Parse(const std::uint8_t* bytes, int32_t length,
                       ChassisDetail* chassis) const {
  chassis->mutable_agribot()->mutable_accel_rpt_2e3()->set_speed_mps(
      speed_mps_value(bytes, length));
  chassis->mutable_agribot()->mutable_accel_rpt_2e3()->set_steering_angular(
      steering_angular_value(bytes, length));
  chassis->mutable_agribot()->mutable_accel_rpt_2e3()->set_motion_direction(
      motion_direction_value(bytes, length));
  chassis->mutable_agribot()->mutable_accel_rpt_2e3()->set_speed_mps_percentage(
      speed_mps_percentage_value(bytes, length));
}

double Accelrpt2e3::speed_mps_value(const std::uint8_t* bytes,
                                    int32_t length) const {
  Byte t0(bytes + 6);
  int16_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 7);
  int16_t t = t1.get_byte(0, 8);
  t <<= 8;
  x |= t;

  double ret = x * 0.001;
  return ret;
}

double Accelrpt2e3::steering_angular_value(const std::uint8_t* bytes,
                                           int32_t length) const {
  Byte t0(bytes + 1);
  int8_t x = t0.get_byte(0, 8);

  return static_cast<double>(x);
}

double Accelrpt2e3::speed_mps_percentage_value(const std::uint8_t* bytes,
                                               const int32_t length) const {
  Byte t0(bytes);
  uint8_t x = t0.get_byte(0, 8);

  return static_cast<double>(x);
}

uint8_t Accelrpt2e3::motion_direction_value(const std::uint8_t* bytes,
                                            const int32_t length) const {
  Byte t0(bytes + 3);
  uint8_t x = t0.get_byte(1, 2);

  return x;
}

}  // namespace agribot
}  // namespace canbus
}  // namespace apollo
