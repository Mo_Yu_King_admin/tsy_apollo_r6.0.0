/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef MODULES_CANBUS_VEHICLE_AGRIBOT_PROTOCOL_ACCEL_CMD_1E3_H_
#define MODULES_CANBUS_VEHICLE_AGRIBOT_PROTOCOL_ACCEL_CMD_1E3_H_

#include "modules/canbus/proto/chassis_detail.pb.h"
#include "modules/drivers/canbus/can_comm/protocol_data.h"

namespace apollo {
namespace canbus {
namespace agribot {

class Accelcmd1e3 : public ::apollo::drivers::canbus::ProtocolData<
                       ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;

  Accelcmd1e3();

  uint32_t GetPeriod() const override;

  void UpdateData(uint8_t* data) override;

  void Reset() override;

  Accelcmd1e3* set_accel_cmd(double accel_cmd);
  Accelcmd1e3* set_steer_cmd(double steer_cmd);
  Accelcmd1e3* set_decel_cmd(double decel_cmd);
  Accelcmd1e3* set_direction_cmd(uint32_t dir_cmd);

 private:
  void set_p_accel_cmd(uint8_t* data, double accel_cmd);
  void set_p_steer_cmd(uint8_t* data, double steer_cmd);
  void set_p_decel_cmd(uint8_t* data, double decel_cmd);
  void set_p_direction_cmd(uint8_t* data, uint32_t dir_cmd);

 private:
  uint32_t dir_cmd_;
  double accel_cmd_;
  double decel_cmd_;
  double steer_cmd_;
};

}  // namespace agribot
}  // namespace canbus
}  // namespace apollo

#endif  // MODULES_CANBUS_VEHICLE_AGRIBOT_PROTOCOL_ACCEL_CMD_1E3_H_
