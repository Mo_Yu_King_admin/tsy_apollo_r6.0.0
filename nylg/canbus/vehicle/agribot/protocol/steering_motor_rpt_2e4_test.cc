/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agribot/protocol/steering_motor_rpt_2e4.h"

#include "gtest/gtest.h"

namespace apollo {
namespace canbus {
namespace agribot {

class Steeringmotorrpt2e4Test : public ::testing::Test {
 public:
  virtual void SetUp() {}
};

TEST_F(Steeringmotorrpt2e4Test, reset) {
  Steeringmotorrpt2e4 steeringmotor;
  int32_t length = 8;
  ChassisDetail chassis_detail;
  uint8_t bytes[8] = {0x01, 0x02, 0x03, 0x04, 0x11, 0x12, 0x13, 0x14};
  steeringmotor.Parse(bytes, length, &chassis_detail);
  EXPECT_DOUBLE_EQ(chassis_detail.agribot().accel_rpt_2e3()\
.motor_shaft_speed_1(), 513);
  EXPECT_DOUBLE_EQ(chassis_detail.agribot().accel_rpt_2e3()\
.motor_shaft_speed_2(), 1027);
  EXPECT_DOUBLE_EQ(chassis_detail.agribot().accel_rpt_2e3()\
.motor_shaft_speed_3(), 4625);
  EXPECT_DOUBLE_EQ(chassis_detail.agribot().accel_rpt_2e3()\
.motor_shaft_speed_3(), 5139);
}

}  // namespace agribot
}  // namespace canbus
}  // namespace apollo

