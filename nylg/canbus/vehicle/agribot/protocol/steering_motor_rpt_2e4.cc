/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agribot/protocol/steering_motor_rpt_2e4.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace agribot {

using ::apollo::drivers::canbus::Byte;

Steeringmotorrpt2e4::Steeringmotorrpt2e4() {}
const int32_t Steeringmotorrpt2e4::ID = 0x2e4;

void Steeringmotorrpt2e4::Parse(const std::uint8_t* bytes, int32_t length,
                                ChassisDetail* chassis) const {
  chassis->mutable_agribot()->mutable_accel_rpt_2e3()->set_motor_shaft_speed_1(
      shaft_speed_up_left(bytes, length));
  chassis->mutable_agribot()->mutable_accel_rpt_2e3()->set_motor_shaft_speed_2(
      shaft_speed_up_right(bytes, length));
  chassis->mutable_agribot()->mutable_accel_rpt_2e3()->set_motor_shaft_speed_3(
      shaft_speed_bottom_left(bytes, length));
  chassis->mutable_agribot()->mutable_accel_rpt_2e3()->set_motor_shaft_speed_4(
      shaft_speed_bottom_right(bytes, length));
}

double Steeringmotorrpt2e4::shaft_speed_bottom_right(const std::uint8_t* bytes,
                                                     int32_t length) const {
  Byte t0(bytes + 6);
  uint32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 7);
  uint32_t t = t1.get_byte(0, 8);
  t <<= 8;
  x |= t;

  return x;
}

double Steeringmotorrpt2e4::shaft_speed_bottom_left(const std::uint8_t* bytes,
                                                    int32_t length) const {
  Byte t0(bytes + 4);
  uint32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 5);
  uint32_t t = t1.get_byte(0, 8);
  t <<= 8;
  x |= t;

  return x;
}

double Steeringmotorrpt2e4::shaft_speed_up_right(const std::uint8_t* bytes,
                                                  int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 8);
  
  Byte t1(bytes +3);
  int32_t t = t1.get_byte(0, 8);
  t <<= 8;
  x |= t;
  
  return x;
}

double Steeringmotorrpt2e4::shaft_speed_up_left(const std::uint8_t* bytes,
                                                 int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 1);
  int32_t t = t1.get_byte(0, 8);
  t <<= 8;
  x |= t;

  return x;
}
}  // namespace agribot
}  // namespace canbus
}  // namespace apollo
