/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agribot/protocol/accel_rpt_2e3.h"

#include "gtest/gtest.h"

namespace apollo {
namespace canbus {
namespace agribot {

class Accelrpt2e3Test : public ::testing::Test {
 public:
  virtual void SetUp() {}
};

TEST_F(Accelrpt2e3Test, reset) {
  Accelrpt2e3 acc;
  int32_t length = 8;
  ChassisDetail chassis_detail;
  uint8_t bytes[8] = {0x01, 0x02, 0x03, 0x05, 0x11, 0x12, 0x13, 0x14};

  acc.Parse(bytes, length, &chassis_detail);
  EXPECT_DOUBLE_EQ(chassis_detail.agribot().accel_rpt_2e3().speed_mps(), 5.139);
  EXPECT_DOUBLE_EQ(chassis_detail.agribot().accel_rpt_2e3().steering_angular(), 2.0);
  EXPECT_EQ(chassis_detail.agribot().accel_rpt_2e3().motion_direction(), 2);
  EXPECT_DOUBLE_EQ(chassis_detail.agribot().accel_rpt_2e3().speed_mps_percentage(), 1.0);
}

}  // namespace agribot
}  // namespace canbus
}  // namespace apollo
