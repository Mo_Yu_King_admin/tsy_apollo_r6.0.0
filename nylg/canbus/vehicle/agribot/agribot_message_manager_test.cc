/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agribot/agribot_message_manager.h"

#include "gtest/gtest.h"

#include "modules/canbus/vehicle/agribot/protocol/accel_cmd_1e3.h"

#include "modules/canbus/vehicle/agribot/protocol/accel_rpt_2e3.h"
#include "modules/canbus/vehicle/agribot/protocol/steering_motor_rpt_2e4.h"

namespace apollo {
namespace canbus {
namespace agribot {

class AgribotMessageManagerTest : public ::testing::Test {
 public:
  AgribotMessageManagerTest() : manager_() {}
  virtual void SetUp() {}

 protected:
  AgribotMessageManager manager_;
};

TEST_F(AgribotMessageManagerTest, GetSendProtocols) {
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Accelcmd1e3::ID) != nullptr);
}

TEST_F(AgribotMessageManagerTest, GetRecvProtocols) {
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Accelrpt2e3::ID) != nullptr);
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Steeringmotorrpt2e4::ID) !=
              nullptr);
}

}  // namespace agribot
}  // namespace canbus
}  // namespace apollo
