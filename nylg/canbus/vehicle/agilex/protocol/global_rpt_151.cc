/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agilex/protocol/global_rpt_151.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace agilex {

using ::apollo::drivers::canbus::Byte;

Globalrpt151::Globalrpt151() {}
const int32_t Globalrpt151::ID = 0x151;

void Globalrpt151::Parse(const std::uint8_t* bytes, int32_t length,
                        ChassisDetail* chassis) const {
  chassis->mutable_agilex()->mutable_global_rpt_151()->set_vehicle_status(
      vehicle_status(bytes, length));
  chassis->mutable_agilex()->mutable_global_rpt_151()->set_mode_status(
      mode_status(bytes, length));
  chassis->mutable_agilex()->mutable_global_rpt_151()->set_battery_voltage(
      battery_voltage(bytes, length));
  chassis->mutable_agilex()->mutable_global_rpt_151()->set_error_status(
      error_status(bytes, length));
}

Global_rpt_151::Vehicle_statusType Globalrpt151::vehicle_status(
    const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 0);
  int32_t x = t0.get_byte(0, 8);

  Global_rpt_151::Vehicle_statusType ret =
      static_cast<Global_rpt_151::Vehicle_statusType>(x);
  return ret;
}

Global_rpt_151::Mode_statusType Globalrpt151::mode_status(
    const std::uint8_t* bytes, int32_t length) const {
  Byte t0(bytes + 1);
  int32_t x = t0.get_byte(0, 8);

  Global_rpt_151::Mode_statusType ret =
      static_cast<Global_rpt_151::Mode_statusType>(x);
  return ret;
}

double Globalrpt151::battery_voltage(const std::uint8_t* bytes,
                                     int32_t length) const {
  Byte t0(bytes + 2);
  uint32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 3);
  uint32_t t = t1.get_byte(0, 8);  
  x <<= 8;
  x |= t;
  
  return x * 0.1;
}

uint32_t Globalrpt151::error_status(const std::uint8_t* bytes,
                                    int32_t length) const {
  Byte t0(bytes + 4);
  uint32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 5);
  uint32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  return x;
}
}  // namespace agilex
}  // namespace canbus
}  // namespace apollo
