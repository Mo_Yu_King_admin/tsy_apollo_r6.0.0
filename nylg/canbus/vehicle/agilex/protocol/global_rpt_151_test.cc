/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agilex/protocol/global_rpt_151.h"

#include "gtest/gtest.h"

namespace apollo {
namespace canbus {
namespace agilex {

class Globalrpt151Test : public ::testing::Test {
 public:
  virtual void SetUp() {}
};

TEST_F(Globalrpt151Test, reset) {
  Globalrpt151 globalrpt;
  int32_t length = 8;
  ChassisDetail chassis_detail;
  uint8_t bytes[8] = {0x00, 0x01, 0x03, 0x04, 0x11, 0x12, 0x13, 0x14};
  globalrpt.Parse(bytes, length, &chassis_detail);
  EXPECT_DOUBLE_EQ(chassis_detail.agilex().global_rpt_151()\
.vehicle_status(), 0);
  EXPECT_DOUBLE_EQ(chassis_detail.agilex().global_rpt_151()\
.mode_status(), 1);
  EXPECT_DOUBLE_EQ(chassis_detail.agilex().global_rpt_151()\
.battery_voltage(), 77.2);
  EXPECT_EQ(chassis_detail.agilex().global_rpt_151()\
.error_status(), 0x1112);
}

}  // namespace agilex
}  // namespace canbus
}  // namespace apollo

