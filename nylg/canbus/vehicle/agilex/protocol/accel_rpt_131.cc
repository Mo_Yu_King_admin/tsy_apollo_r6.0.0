/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agilex/protocol/accel_rpt_131.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace agilex {

using ::apollo::drivers::canbus::Byte;

Accelrpt131::Accelrpt131() {}
const int32_t Accelrpt131::ID = 0x131;

void Accelrpt131::Parse(const std::uint8_t* bytes, int32_t length,
                       ChassisDetail* chassis) const {
  chassis->mutable_agilex()->mutable_accel_rpt_131()->set_speed_mps(
      speed_mps_value(bytes, length));
  chassis->mutable_agilex()->mutable_accel_rpt_131()->set_steering_angular(
      steering_angular_value(bytes, length));
}

double Accelrpt131::speed_mps_value(const std::uint8_t* bytes,
                                int32_t length) const {
  Byte t0(bytes + 0);
  int16_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 1);
  int16_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.001000;
  return ret;
}

double Accelrpt131::steering_angular_value(const std::uint8_t* bytes,
                                   int32_t length) const {
  Byte t0(bytes + 2);
  int16_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 3);
  int16_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.001000;
  return ret;
}

}  // namespace agilex
}  // namespace canbus
}  // namespace apollo
