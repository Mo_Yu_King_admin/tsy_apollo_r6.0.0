/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef MODULES_CANBUS_VEHICLE_AGILEX_PROTOCOL_ACCEL_CMD_130_H_
#define MODULES_CANBUS_VEHICLE_AGILEX_PROTOCOL_ACCEL_CMD_130_H_

#include "modules/canbus/proto/chassis_detail.pb.h"
#include "modules/drivers/canbus/can_comm/protocol_data.h"

namespace apollo {
namespace canbus {
namespace agilex {

class Accelcmd130 : public ::apollo::drivers::canbus::ProtocolData<
                       ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;

  Accelcmd130();

  uint32_t GetPeriod() const override;

  void UpdateData(uint8_t* data) override;

  void Reset() override;

  Accelcmd130* set_accel_cmd(double accel_cmd);
  Accelcmd130* set_steer_cmd(double steer_cmd);
  Accelcmd130* set_option_cmd(uint32_t option_cmd);

 private:
  void set_p_accel_cmd(uint8_t* data, double accel_cmd);
  void set_p_steer_cmd(uint8_t* data, double steer_cmd);
  void set_p_option_cmd(uint8_t* data, uint32_t option_cmd);
  void set_p_loop_cmd(uint8_t* data, uint32_t loop_cmd);
  void calc_p_checksum(uint8_t* data);

 private:
  uint32_t option_cmd_;
  double accel_cmd_;
  double steer_cmd_;
  uint32_t loop_cmd_;
};

}  // namespace agilex
}  // namespace canbus
}  // namespace apollo

#endif  // MODULES_CANBUS_VEHICL_AGILEX_PROTOCOL_ACCEL_CMD_67_H_
