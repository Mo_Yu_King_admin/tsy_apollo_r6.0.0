/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agilex/protocol/steering_motor_rpt_3_203.h"

#include "glog/logging.h"

#include "modules/drivers/canbus/common/byte.h"
#include "modules/drivers/canbus/common/canbus_consts.h"

namespace apollo {
namespace canbus {
namespace agilex {

using ::apollo::drivers::canbus::Byte;

Steeringmotorrpt203::Steeringmotorrpt203() {}
const int32_t Steeringmotorrpt203::ID = 0x203;

void Steeringmotorrpt203::Parse(const std::uint8_t* bytes, int32_t length,
                                ChassisDetail* chassis) const {
  chassis->mutable_agilex()
      ->mutable_steering_motor_rpt_3_203()
      ->set_actuator_electricity(motor_current(bytes, length));
  chassis->mutable_agilex()
      ->mutable_steering_motor_rpt_3_203()
      ->set_motor_temperature(motor_temperature(bytes, length));
  chassis->mutable_agilex()->mutable_steering_motor_rpt_3_203()
      ->set_motor_shaft_speed(shaft_speed(bytes, length));
}

double Steeringmotorrpt203::motor_current(const std::uint8_t* bytes,
                                          int32_t length) const {
  Byte t0(bytes + 0);
  uint32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 1);
  uint32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  double ret = x * 0.1000;
  return ret;
}

int32_t Steeringmotorrpt203::motor_temperature(const std::uint8_t* bytes,
                                          int32_t length) const {
  Byte t0(bytes + 4);
  int32_t x = t0.get_byte(0, 8);

  return x;
}

int32_t Steeringmotorrpt203::shaft_speed(const std::uint8_t* bytes,
                                           int32_t length) const {
  Byte t0(bytes + 2);
  int32_t x = t0.get_byte(0, 8);

  Byte t1(bytes + 3);
  int32_t t = t1.get_byte(0, 8);
  x <<= 8;
  x |= t;

  return x;
}
}  // namespace agilex
}  // namespace canbus
}  // namespace apollo
