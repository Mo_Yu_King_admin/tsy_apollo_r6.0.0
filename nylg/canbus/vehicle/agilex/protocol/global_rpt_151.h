/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#ifndef MODULES_CANBUS_VEHICLE_AGILEX_PROTOCOL_GLOBAL_RPT_151_H_
#define MODULES_CANBUS_VEHICLE_AGILEX_PROTOCOL_GLOBAL_RPT_151_H_

#include "modules/canbus/proto/chassis_detail.pb.h"
#include "modules/drivers/canbus/can_comm/protocol_data.h"

namespace apollo {
namespace canbus {
namespace agilex {

class Globalrpt151 : public ::apollo::drivers::canbus::ProtocolData<
                        ::apollo::canbus::ChassisDetail> {
 public:
  static const int32_t ID;
  Globalrpt151();
  void Parse(const std::uint8_t* bytes, int32_t length,
             ChassisDetail* chassis) const override;

 private:
  Global_rpt_151::Vehicle_statusType vehicle_status(const std::uint8_t* bytes,
                                                 const int32_t length) const;

  Global_rpt_151::Mode_statusType mode_status(
      const std::uint8_t* bytes, const int32_t length) const;

  double battery_voltage(const std::uint8_t* bytes, const int32_t length) const;

  uint32_t error_status(const std::uint8_t* bytes,
                          const int32_t length) const;
};

}  // namespace agilex
}  // namespace canbus
}  // namespace apollo

#endif  // MODULES_CANBUS_VEHICL_AGILEX_PROTOCOL_GLOBAL_RPT_151_H_
