/******************************************************************************
 * Copyright 2018 The Apollo Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "modules/canbus/vehicle/agilex/agilex_message_manager.h"

#include "gtest/gtest.h"

#include "modules/canbus/vehicle/agilex/protocol/accel_cmd_130.h"

#include "modules/canbus/vehicle/agilex/protocol/accel_rpt_131.h"
#include "modules/canbus/vehicle/agilex/protocol/global_rpt_151.h"
#include "modules/canbus/vehicle/agilex/protocol/steering_motor_rpt_1_201.h"
#include "modules/canbus/vehicle/agilex/protocol/steering_motor_rpt_2_202.h"
#include "modules/canbus/vehicle/agilex/protocol/steering_motor_rpt_3_203.h"

namespace apollo {
namespace canbus {
namespace agilex {

class AgileXMessageManagerTest : public ::testing::Test {
 public:
  AgileXMessageManagerTest() : manager_() {}
  virtual void SetUp() {}

 protected:
  AgileXMessageManager manager_;
};

TEST_F(AgileXMessageManagerTest, GetSendProtocols) {
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Accelcmd130::ID) != nullptr);
}

TEST_F(AgileXMessageManagerTest, GetRecvProtocols) {
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Accelrpt131::ID) != nullptr);
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Globalrpt151::ID) != nullptr);
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Steeringmotorrpt201::ID) !=
              nullptr);
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Steeringmotorrpt202::ID) !=
              nullptr);
  EXPECT_TRUE(manager_.GetMutableProtocolDataById(Steeringmotorrpt203::ID) !=
              nullptr);
}

}  // namespace agilex
}  // namespace canbus
}  // namespace apollo
