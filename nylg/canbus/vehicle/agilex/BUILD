load("//tools:cpplint.bzl", "cpplint")

package(default_visibility = ["//visibility:public"])

cc_library(
    name = "agilex_vehicle_factory",
    srcs = [
        "agilex_vehicle_factory.cc",
    ],
    hdrs = [
        "agilex_vehicle_factory.h",
    ],
    deps = [
        ":agilex_controller",
        ":agilex_message_manager",
        "//modules/canbus/vehicle:abstract_vehicle_factory",
    ],
)

cc_test(
    name = "agilex_vehicle_factory_test",
    size = "small",
    srcs = ["agilex_vehicle_factory_test.cc"],
    deps = [
        ":agilex_vehicle_factory",
        "@gtest//:main",
    ],
)

cc_library(
    name = "agilex_message_manager",
    srcs = [
        "agilex_message_manager.cc",
    ],
    hdrs = [
        "agilex_message_manager.h",
    ],
    deps = [
        "//modules/drivers/canbus/common:canbus_common",
        "//modules/canbus/proto:canbus_proto",
        "//modules/drivers/canbus/can_comm:message_manager_base",
        "//modules/canbus/vehicle/agilex/protocol:canbus_agilex_protocol",
    ],
)

cc_test(
    name = "agilex_message_manager_test",
    size = "small",
    srcs = [
        "agilex_message_manager_test.cc",
    ],
    deps = [
        ":agilex_message_manager",
        "@gtest//:main",
    ],
)

cc_library(
    name = "agilex_controller",
    srcs = [
        "agilex_controller.cc",
    ],
    hdrs = [
        "agilex_controller.h",
    ],
    deps = [
        ":agilex_message_manager",
        "//modules/drivers/canbus/can_comm:can_sender",
        "//modules/drivers/canbus/common:canbus_common",
        "//modules/canbus/proto:canbus_proto",
        "//modules/drivers/canbus/can_comm:message_manager_base",
        "//modules/canbus/vehicle:vehicle_controller_base",
        "//modules/canbus/vehicle/agilex/protocol:canbus_agilex_protocol",
    ],
)

cc_test(
    name = "agilex_controller_test",
    size = "small",
    data = ["//modules/canbus:canbus_testdata"],
    srcs = [
        "agilex_controller_test.cc",
    ],
    deps = [
        ":agilex_controller",
        "//modules/common/util:util",
        "@gtest//:main",
    ],
)

cpplint()
