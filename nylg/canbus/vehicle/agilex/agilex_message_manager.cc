/* Copyright 2018 The Apollo Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include "modules/canbus/vehicle/agilex/agilex_message_manager.h"

#include "modules/canbus/vehicle/agilex/protocol/accel_cmd_130.h"

#include "modules/canbus/vehicle/agilex/protocol/accel_rpt_131.h"
#include "modules/canbus/vehicle/agilex/protocol/global_rpt_151.h"
#include "modules/canbus/vehicle/agilex/protocol/steering_motor_rpt_1_201.h"
#include "modules/canbus/vehicle/agilex/protocol/steering_motor_rpt_2_202.h"
#include "modules/canbus/vehicle/agilex/protocol/steering_motor_rpt_3_203.h"

namespace apollo {
namespace canbus {
namespace agilex {

AgileXMessageManager::AgileXMessageManager() {
  // Control Messages
  AddSendProtocolData<Accelcmd130, true>();

  // Report Messages
  AddRecvProtocolData<Accelrpt131, true>();
  AddRecvProtocolData<Globalrpt151, true>();
  AddRecvProtocolData<Steeringmotorrpt201, true>();
  AddRecvProtocolData<Steeringmotorrpt202, true>();
  AddRecvProtocolData<Steeringmotorrpt203, true>();
}

AgileXMessageManager::~AgileXMessageManager() {}

}  // namespace agilex
}  // namespace canbus
}  // namespace apollo
